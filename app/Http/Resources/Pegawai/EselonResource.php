<?php

namespace App\Http\Resources\Pegawai;

use Illuminate\Http\Resources\Json\JsonResource;

class EselonResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return isset($this->id) ? [
            'id' => $this->id,
            'nama' => $this->nama,
            'kode' => $this->kode,
        ] : (object) [];
    }
}
