<?php

namespace App\Http\Resources\Pegawai;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Master\JabatanResource;
use App\Model\User;

class ProfilPegawaiResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = User::where('nip', $this->nip)->first();
        return [
            'nip' => $this->nip,
            'level' => $user->level,
            'nama' => $this->nama,
            'lahir' => (object) [
                'tempat' => $this->tempat_lahir,
                'tanggal' => $this->tanggal_lahir
            ],
            'satuanKerja' => new SatuanKerjaResource($this->satuanKerja),
            'statusKepegawaian' => new StatusKepegawaianResource($this->statusKepegawaian),
            'jenisKelamin' => $this->jk === 1 ? 'Laki-laki' : 'Perempuan',
            'instansi' => $this->instansi,
            // 'eselon' => new EselonResource($this->eselon),
            'jabatan' => new JabatanResource($this->jabatan)
        ];
    }
}
