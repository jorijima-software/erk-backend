<?php

namespace App\Http\Resources\Pegawai;

use Illuminate\Http\Resources\Json\JsonResource;

class StrukturResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'nama' => $this->nama,
            'pegawai' => $this->pegawai ? PegawaiDaftarResource::collection($this->pegawai) : [],
            'jenisJabatan' => $this->jenisJabatan->nama
        ];

        if(isset($this->bawahan) && $this->bawahan->count() > 0 && $this->pegawai->count() > 0) {
            $data['bawahan'] = StrukturResource::collection($this->bawahan);
        }

        return $data;
    }
}
