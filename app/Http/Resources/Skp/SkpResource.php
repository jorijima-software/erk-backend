<?php

namespace App\Http\Resources\Skp;

use Illuminate\Http\Resources\Json\JsonResource;

class SkpResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nama' => $this->nama,
            'waktu' => $this->waktu,
            'tipe_waktu' => $this->tipeWaktu,
            'output' => $this->output,
            'tipe_output' => $this->tipeOutput,
            'kegiatan' => $this->kegiatan,
            'tupoksi' => $this->tupoksi,
            'biaya' => $this->biaya > 0 ? $this->biaya : false
        ];
    }
}
