<?php

namespace App\Http\Resources\Aktifitas;

use Illuminate\Http\Resources\Json\JsonResource;

class AktifitasResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nama' => $this->nama,
            'kegiatan' => $this->kegiatan ? new KegiatanResource($this->kegiatan) : false,
            'tupoksi' => $this->tupoksi ? new TupoksiResource($this->tupoksi) : false,
            'waktu' => "{$this->waktu} {$this->tipeWaktu->nama}",
            'output' => "{$this->output} {$this->tipeOutput->nama}",
            'biaya' => $this->biaya > 0 ? $this->biaya : false,
            'kualitas' => $this->kualitas
        ];
    }
}
