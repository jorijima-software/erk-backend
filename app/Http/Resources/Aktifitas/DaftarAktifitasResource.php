<?php

namespace App\Http\Resources\Aktifitas;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Skp\SkpResource;

class DaftarAktifitasResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $status = [
            0 => 'Ditolak',
            1 => 'Diproses',
            2 => 'Diterima'
        ];

        return [
            'id' => $this->id,
            'nama' => $this->nama,
            'skp' => new SkpResource($this->skp),
            'waktuMulai' => $this->waktu_mulai,
            'waktuSelesai' => $this->waktu_selesai,
            'keterangan' => $this->keterangan,
            'status' => $status[$this->status] ?? 'Diproses'
        ];
    }
}
