<?php

namespace App\Http\Resources\Master;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Master\Jabatan\AtasanResource;

class JabatanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nama' => $this->nama,
            'eselon' => new EselonResource($this->eselon),
            'atasan' => new AtasanResource($this->atasan),
            'bawahan' => new AtasanResource($this->bawahan),
            'jenis' => $this->jenisJabatan
        ];
    }
}
