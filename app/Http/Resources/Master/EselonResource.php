<?php

namespace App\Http\Resources\Master;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Model\Master\Eselon;

class EselonResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $eselon = new Eselon();
        return [
            'id' => $this->id,
            'nama' => $this->nama,
            'golongan' => $eselon->golongan([$this->golongan_awal_id, $this->golongan_akhir_id]),
        ];
    }
}
