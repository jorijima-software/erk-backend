<?php

namespace App\Http\Resources\Master\Jabatan;

use Illuminate\Http\Resources\Json\JsonResource;

class AtasanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nama' => $this->nama
        ];
    }
}
