<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Model\Pegawai;
use Tymon\JWTAuth\Facades\JWTFactory;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    protected $validatorMessages = [
        'nip.required' => 'Mohon isi nip.',
        'password.required' => 'Password wajib diisi.'
    ];

    public function __construct() {
        $this->middleware('\App\Http\Middleware\VerifyJWTToken')->except(['login']);
    }

    /**
    * @SWG\Post(
    *      path="/authentication",
    *      operationId="userAuthentication",
    *      tags={"Login"},
    *      summary="Autentikasi User dan Token",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           in="body",
    *           name="body",
    *           description="Isi Request",
    *           required=true,
    *           @SWG\Schema(
    *               type="object",
    *               @SWG\Property(property="nip", type="string"),
    *               @SWG\Property(property="password", type="string", default="123456"),
    *               required={"nip", "password"}
    *           )
    *       ),
    *     )
    */
    public function login(Request $req) {
        $validator = Validator::make(request()->all(), [
            'nip' => 'required',
            'password' => 'required',
        ], $this->validatorMessages);

        if($validator->fails()) {
            return $this->errorMessage($this->validatorMessage($validator->errors()));
        }

        $credentials = $req->only('nip', 'password');

        $auth = JWTAuth::attempt($credentials);

        if($auth) {
            return response()->json([
                'token' => $auth,
                'success' => true,
                'code' => 200
            ]);
        } else if($this->checkLocalUser($req->input('nip'))) {
            return $this->errorMessage('Username atau password salah.', 401);
        } else {
            $nip = $req->input('nip');
            $user = $this->checkUserFromApi($nip);
            if(isset($user->error)) {
                return $this->errorMessage($user->message, $user->code);
            } else {
                $auth = JWTAuth::attempt($credentials);

                return response()->json([
                    'token' => $auth,
                    'success' => true,
                    'code' => 200
                ]);
            }
        }
    }

    /**
    * @SWG\Post(
    *      path="/change-password",
    *      operationId="changePassword",
    *      tags={"Ganti Password"},
    *      summary="Ganti Password User",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           in="body",
    *           name="body",
    *           description="Isi Request",
    *           required=true,
    *           @SWG\Schema(
    *               type="object",
    *               @SWG\Property(property="password", type="string", default="123456"),
    *               @SWG\Property(property="newPassword", type="string", default="123456"),
    *               @SWG\Property(property="newPasswordVerify", type="string", default="123456"),
    *               required={"password", "newPassword", "newPasswordVerify"}
    *           )
    *       ),
    *     )
    */
    public function changePassword(Request $req) {
        $user = JWTAuth::parseToken()->toUser();

        $validator = Validator::make($req->all(), [
            'password' => 'required',
            'newPassword' => 'required',
            'newPasswordVerify' => 'required',
        ]);
        
        if($validator->fails()) {
            return $this->errorMessage($this->validatorMessage($validator->errors()));
        }

        if(!password_verify($req->input('password'), $user->password)) {
            return $this->errorMessage('Kata Sandi tidak cocok', 401);
        }

        if($req->input('newPassword') != $req->input('newPasswordVerify')) {
            return $this->errorMessage('Verifikasi kata sandi tidak cocok', 400);
        }

        $user->password = Hash::make($req->input('newPassword'));
        $user->save();
        
        return $this->successMessage();
    }

    private function checkLocalUser($nip) {
        return User::where('nip', $nip)->first();
    }

    private function checkUserFromApi($nip) {
        $pegawai = new Pegawai();
        return $pegawai->checkUserFromApi($nip);
    }

    private function guard() {
        return Auth::guard();
    }
}
