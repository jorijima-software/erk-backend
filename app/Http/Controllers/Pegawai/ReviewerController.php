<?php

namespace App\Http\Controllers\Pegawai;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Model\Reviewer;
use Illuminate\Support\Facades\Validator;
use App\Model\Master\Jabatan;
use App\Model\Pegawai;
use App\Http\Resources\Pegawai\PegawaiDaftarResource;

class ReviewerController extends Controller
{
    public function __construct() {
        $this->middleware(\App\Http\Middleware\VerifyJWTToken::class);
    }

    /**
    * @SWG\Get(
    *      path="/pegawai/reviewer",
    *      operationId="daftarPegawaiYangSudahMemiliReviewer",
    *      tags={"Daftar"},
    *      summary="Daftar Pegawai yang Sudah Memiliki Reviewer atau Mereview",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Kode Satuan Kerja",
    *           in="query",
    *           name="kodeSatker"
    *       ),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Pencarian",
    *           in="query",
    *           name="search"
    *       ),
    *       security={"JWT": {}}
    *     )
    */
    public function index(Request $req) {
        $user = JWTAuth::parseToken()->toUser();

        $kodeSatker = $req->input('kodeSatker');
        $search = $req->input('search');

        $validator = Validator::make($req->all(), [
            'kodeSatker' => $user->level < 2 ? 'required' : '',
        ], ['kodeSatker.required' => 'Kode Satuan Kerja dibutuhkan.']);

        if($validator->fails()) {
            return $this->errorMessage($this->validatorMessage($validator->errors()));
        }

        if($user->level > 2) {
            return $this->errorMessage('Anda tidak mempunyai hak akses.', 401);
        }


        $instance = Reviewer::take(10)->reviewer($user, $kodeSatker, $search);

        $total = $instance->count();
        $result = $instance->get();

        $data = [
            'reviewer' => $result,
            'belumMereview' => $this->jumlahTidakMereview($user, $kodeSatker)
        ];

        return $this->successMessageWithData($data, $total);
    }

    /**
    * @SWG\Get(
    *      path="/pegawai/belum-mereview",
    *      operationId="daftarPegawaiYangBelumMereview",
    *      tags={"Daftar"},
    *      summary="Daftar Pegawai yang Belum Mereview di Satuan Kerja",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Kode Satuan Kerja",
    *           in="query",
    *           name="kodeSatker"
    *       ),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Pencarian",
    *           in="query",
    *           name="search"
    *       ),
    *       security={"JWT": {}}
    *     )
    */
    /**
     * Fungsi Untuk Mencari Pejabat yang Belum Mempunyai Reviewer
     * @param Request $req
     * @return \Illuminate\Http\JsonResponse
     */
    public function belumMereview(Request $req) {
        $user = JWTAuth::parseToken()->toUser();

        $kodeSatker = $req->input('kodeSatker');
        $search = $req->input('search');

        $validator = Validator::make($req->all(), [
            'kodeSatker' => $user->level < 2 ? 'required' : '',
        ], ['kodeSatker.required' => 'Kode Satuan Kerja dibutuhkan.']);

        if($validator->fails()) {
            return $this->errorMessage($this->validatorMessage($validator->errors()));
        }

        if($user->level > 2) {
            return $this->errorMessage('Anda tidak mempunyai hak akses.', 401);
        }

        $instance = Reviewer::withoutReviewer($user, $kodeSatker);
        $total = $instance->count();
        $result = $instance->get();

        return $this->successMessageWithData($result, $total);
    }


    public function jumlahTidakMereview($user, $kode) {
        return Reviewer::noReviewer($user, $kode)->count();
    }

    /**
    * @SWG\Get(
    *      path="/reviewer/{nip}/atasan",
    *      operationId="daftarReviewerAtasan",
    *      tags={"Daftar"},
    *      summary="Daftar Reviewer Atasan",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Nomor Induk Pegawai yang akan direview",
    *           in="path",
    *           name="nip"
    *       ),
    *       security={"JWT": {}}
    *     )
    */
    public function daftarReviewerAtasan($nip, Request $req) {
        $instance = Jabatan::whereHas('pegawai', function($subQuery) use ($nip) {
            $subQuery->where('nip', $nip);
        })->first();

        if(!$instance || !$instance->atasan) {
            return $this->successMessageWithData([], 0);
        }

        $reviewer = Reviewer::where('nip_reviewer', $nip)->pluck('nip');

        $atasan = $instance->atasan->pegawai;
        $atasan = $atasan->whereNotIn('nip',  $reviewer);

        $atasan = PegawaiDaftarResource::collection($atasan);
        $total = $atasan->count();

        return $this->successMessageWithData($atasan, $total);
    }

    /**
    * @SWG\Get(
    *      path="/reviewer/{nip}/bawahan",
    *      operationId="daftarReviewerBawahan",
    *      tags={"Daftar"},
    *      summary="Daftar Reviewer Bawahan",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Nomor Induk Pegawai yang akan direview",
    *           in="path",
    *           name="nip"
    *       ),
    *       security={"JWT": {}}
    *     )
    */
    public function daftarReviewerBawahan($nip, Request $req) {
        $instance = Jabatan::whereHas('pegawai', function($subQuery) use ($nip) {
            $subQuery->where('nip', $nip);
        })->first();

        if(!$instance || !$instance->bawahan) {
            return $this->successMessageWithData([], 0);
        }

        $reviewer = Reviewer::where('nip_reviewer', $nip)->pluck('nip');

        $bawahan = $instance->bawahan->pegawai;
        $bawahan = $bawahan->whereNotIn('nip',  $reviewer);

        $bawahan = PegawaiDaftarResource::collection($bawahan);
        $total = $bawahan->count();

        return $this->successMessageWithData($bawahan, $total);
    }

    /**
    * @SWG\Get(
    *      path="/reviewer/{nip}/rekan",
    *      operationId="daftarReviewerRekanKerja",
    *      tags={"Daftar"},
    *      summary="Daftar Reviewer Rekan Kerja",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Nomor Induk Pegawai yang akan direview",
    *           in="path",
    *           name="nip"
    *       ),
    *       security={"JWT": {}}
    *     )
    */
    public function daftarReviewerRekanKerja($nip, Request $req) {
        $pegawai = Pegawai::where('nip', $nip)->first();
        $reviewer = Reviewer::where('nip_reviewer', $nip)->pluck('nip');

        $instance = Pegawai::where('jabatan_id', $pegawai->jabatan_id)->where('nip', '!=', $nip)->whereNotIn('nip', $reviewer);

        $total = $instance->count();
        $result = PegawaiDaftarResource::collection($instance->get());
        return $this->successMessageWithData($result, $total);

    }

    /**
    * @SWG\Post(
    *      path="/reviewer/{nip}",
    *      operationId="masterSetReviewer",
    *      tags={"Set Reviewer"},
    *      summary="Set Reviewer",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(in="path", required=true, name="nip", type="string", description="Nomor Induk Pegawai yang Akan Di Set Reviewer nya"),
    *       @SWG\Parameter(
    *           in="body",
    *           name="body",
    *           description="Set Reviewer",
    *           required=true,
    *           @SWG\Schema(
    *               type="object",
    *               @SWG\Property(property="nip", type="string", description="Nomor induk pegawai yang mereview"),
    *               @SWG\Property(property="bulan", type="number", description="Bulan"),
    *               required={"nip", "bulan"}
    *           ),
    *       ),
    *       security={{
    *           "JWT": {}
    *       }}
    *     )
    */
    public function setReviewer($nip, Request $req) {
        $pegawai = Pegawai::where('nip', $nip)->first();

        if(!$pegawai) {
            return $this->notFoundErrorMessage();
        }

        $validator = Validator::make($req->all(), [
            'nip' => 'required',
            'bulan' => 'required'
        ], [
            'nip.required' => 'Pegawai wajib dipilih!',
            'bulan.required' => 'Bulan wajib dipilih',
        ]);

        if($validator->fails()) {
            return $this->errorMessage($this->validatorMessage($validator->errors()));
        }

        $bulanArray = [1,2,3,4,5,6,7,8,9,10,11,12];

        if(!in_array($req->input('bulan'), $bulanArray)) {
            return $this->errorMessage('Bulan tidak valid!');
        }

        try {
            $insert = new Reviewer();
            $insert->nip = $nip;
            $insert->nip_reviewer = $req->input('nip');
            $insert->bulan = $req->input('bulan');
            $insert->save();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }

}
