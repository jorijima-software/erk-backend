<?php

namespace App\Http\Controllers\Pegawai;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use App\Model\Pegawai;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Resources\Pegawai\ProfilPegawaiResource;
use App\Http\Resources\Pegawai\StrukturResource;
use App\Model\Master\Jabatan;
use App\Model\InputAktifitas;
use App\Http\Resources\Aktifitas\DaftarAktifitasResource;

class PegawaiController extends Controller
{
    public function __construct()
    {
        $this->middleware(\App\Http\Middleware\VerifyJWTToken::class);
        // ->except(['strukturPegawai']);
    }

    /**
    * @SWG\Get(
    *      path="/pegawai/profile",
    *      operationId="profilePegawai",
    *      tags={"Profile"},
    *      summary="Profile Pegawai",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Response(response=401, description="User Belum Login"),
    *       security={"JWT": {}}
    *     )
    */
    public function profile(Request $req) {
        $user = JWTAuth::parseToken()->toUser();

        if(!$user) {
            return $this->notFoundErrorMessage();
        }

        $pegawai = Pegawai::where('nip', $user->nip)->first();

        if(!$pegawai) {
            return $this->successMessageWithData((object) [
                'username' => $user->nip,
                'level' => $user->level,
            ]);
        }

        return $this->successMessageWithData(new ProfilPegawaiResource($pegawai));
    }

    public function strukturPegawai($id = null, Request $req) {
        $user = JWTAuth::parseToken()->toUser();

        // if(!$user || $user->level > 2) {
        //     return $this->errorMessage('Anda tidak berhak mengakses halaman ini.', 401);
        // }

        if(!$id) {
            $pegawai = Pegawai::where('nip', $user->nip)->first();
            $jabatan = Jabatan::find($pegawai->jabatan_id);
        } else {
            $jabatan = Jabatan::find($id);
        }

        return $jabatan ? new StrukturResource($jabatan) : $this->errorMessage('Tidak dapat mendapatkan jabatan.', 404);
    }

    public function daftarAktifitas(Request $req) {
        $user = JWTAuth::parseToken()->toUser();

        if(!$user) {
            return $this->errorMessage('Pengguna tidak dapat diverifikasi, mohon login kembali.', 401);
        }

        $sort = $req->input('sort');
        $sortBy = $req->input('sortBy');
        $limit  = $req->input('limit') ?? 10;
        $page = $req->input('page');
        $search = $req->input('search');

        $sorter = ['nama', 'waktu_mulai', 'waktu_selesai', 'status', 'skp_id'];

        $instance = InputAktifitas::take($limit)
            ->when(in_array($sortBy, $this->sortable) && in_array($sort, $sorter), function($q) use ($sort, $sortBy) {
                $q->where($sort, $sortBy);
            })
            ->when(strlen($search) > 0, function($query) use ($search) {
                $query->where(function($q) use ($search) {
                    $q
                        ->where('nama', 'like', "%{$search}%")
                    ;
                });
            })
            ->when($user->level > 1, function($q) use ($user) {
                $q->where('nip', $user->nip);
            })
        ;

        $total = $instance->count();

        $instance->when($page > 0, function($q) use ($page, $limit) {
            $skip = ($page - 1) * $limit;
            $q->skip($skip);
        });

        $result = DaftarAktifitasResource::collection($instance->get());

        return $this->successMessageWithData($result, $total);
    }
}
