<?php

namespace App\Http\Controllers\Skp;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Skp;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\Skp\SkpResource;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Model\Pegawai;

class SkpController extends Controller
{
    private $sorter = [];

    public function __construct() {
        $this->middleware('App\Http\Middleware\VerifyJWTToken');
    }

    /**
    * @SWG\Get(
    *      path="/skp/{skpId}",
    *      operationId="skpDetail",
    *      tags={"Detail"},
    *      summary="Detail Sasaran Kerja Pegawai",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           in="path",
    *           name="skpId",
    *           description="ID SKP",
    *           required=true,
    *           type="number"
    *       ),
    *       security={"JWT": {}}
    *     )
    */
    public function show(Skp $skp) {
        if(!$skp) {
            return $this->notFoundErrorMessage();
        }

        $result = new SkpResource($skp);

        return $this->successMessageWithData($result);
    }

    /**
    * @SWG\Get(
    *      path="/skp",
    *      operationId="daftarSKP",
    *      tags={"Daftar"},
    *      summary="Daftar Sasaran Kerja Pegawai",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Mengurutkan data berdasarkan nama",
    *           in="query",
    *           name="sort",
    *           enum={"desc", "asc"},
    *       ),
    *       @SWG\Parameter(
    *           type="number",
    *           description="Membatasi banyaknya data yang ditampilkan",
    *           in="query",
    *           name="limit"
    *       ),
    *       @SWG\Parameter(
    *           type="number",
    *           description="Halaman",
    *           in="query",
    *           name="page"
    *       ),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Pencarian data berdasarkan nama",
    *           in="query",
    *           name="search"
    *       ),
    *       security={"JWT": {}}
    *     )
    */
    public function index(Request $req) {
        $user = JWTAuth::parseToken()->toUser();
        $pegawai = Pegawai::where('nip', $user->nip)->first();

        $sort = $req->input('sort');
        $sortBy = $req->input('sortBy');
        $limit = $req->input('limit') ?? 10;
        $page = $req->input('page');
        $search = $req->input('search');

        $instance = Skp::when(in_array($sortBy, $this->sortable) && in_array($sortBy, $this->sorter), function ($q) use ($sort, $sortBy) {
                $q->orderBy($sort, $sortBy);
            })
            ->when($user->level > 2, function($q) use ($user) {
                $q->where('nip', $user->nip);
            })
            ->when(strlen($search) > 0, function($q) use ($search) {
                $q->where('nama', 'like', "%${search}%");
            })
            ->take($limit)
        ;

        $total = $instance->count();

        $instance->when($page > 0, function($q) use ($page, $limit) {
            $skip = ($page - 1) * $limit;
            $q->skip($skip);
        });

        $result = SkpResource::collection($instance->get());

        return $this->successMessageWithData($result, $total);
    }

    /**
    * @SWG\Post(
    *      path="/skp",
    *      operationId="skp",
    *      tags={"Tambah"},
    *      summary="Tambah Sasaran Kerja Pegawai",
    *      @SWG\Response(
    *          response=200,
    *          description="Berhasil Menambah Data"
    *       ),
    *       @SWG\Response(response=400, description="Input Salah, atau Kurang"),
    *       @SWG\Response(response=401, description="Pengguna Belum Login/Mendapatkan Token"),
    *       @SWG\Parameter(
    *           in="body",
    *           name="body",
    *           description="Tambah SKP",
    *           required=true,
    *           @SWG\Schema(
    *               type="object",
    *               @SWG\Property(property="nama", type="string", description="Nama SKP"),
    *               @SWG\Property(property="nip", type="string", description="NIP (Only required when logged as admin or operator)"),
    *               @SWG\Property(property="kegiatan", type="number", description="ID Kegiatan"),
    *               @SWG\Property(property="tupoksi", type="number", description="ID Tupoksi"),
    *               @SWG\Property(property="waktu", type="number", description="Waktu yang diperlukan"),
    *               @SWG\Property(property="tipeWaktu", type="number", description="ID Tipe Waktu"),
    *               @SWG\Property(property="output", type="number", description="Banyaknya Output yang diharapkan"),
    *               @SWG\Property(property="tipeOutput", type="number", description="ID Tipe Output SKP"),
    *               @SWG\Property(property="biaya", type="number", description="ID Tipe Output SKP"),
    *               required={"nama", "waktu", "tipeWaktu", "output", "tipeOutput"}
    *           )
    *        ),
    *       security={"JWT": {}}
    *     )
    */
    public function store(Request $req) {
        $user = JWTAuth::parseToken()->toUser();

        $validator = Validator::make($req->all(), [
            'nip' => $user->level <= 2 ? 'required' : '',
            'nama' => 'required',
            'waktu' => 'required',
            'tipeWaktu' => 'required',
            'output' => 'required',
            'tipeOutput' => 'required'
        ]);

        if($validator->fails()) {
            return $this->errorMessage($this->validatorMessage($validator->errors()));
        }

        if($req->input('kegiatan') && (!$req->input('biaya') || $req->input('biaya') <= 0)) {
            return $this->errorMessage('Biaya wajib diisi!');
        }

        try {
            $instance = new Skp();
            $instance->nama = $req->input('nama');
            $instance->nip = $user->level <= 2 ? $req->input('nip') : $user->nip;
            $instance->tupoksi_id = !empty($req->input('tupoksi')) ? $req->input('tupoksi') : null;
            $instance->kegiatan_id = !empty($req->input('kegiatan')) ? $req->input('kegiatan') : null;
            $instance->waktu = $req->input('waktu');
            $instance->tipe_waktu_id = $req->input('tipeWaktu');
            $instance->output = $req->input('output');
            $instance->skp_output_id = $req->input('tipeOutput');
            $instance->biaya = $req->input('biaya');
            $instance->kualitas = 100;
            $instance->save();

            return $this->successMessage();
        } catch(\Exceptions $e) {
            return $this->errorMessage($e->getMessages(), 500);
        }
    }

    /**
    * @SWG\Patch(
    *      path="/skp/{skpId}",
    *      operationId="skp",
    *      tags={"Update"},
    *      summary="Update Sasaran Kerja Pegawai",
    *      @SWG\Response(
    *          response=200,
    *          description="Berhasil Menambah Data"
    *       ),
    *       @SWG\Response(response=400, description="Input Salah, atau Kurang"),
    *       @SWG\Response(response=401, description="Pengguna Belum Login/Mendapatkan Token"),
    *       @SWG\Parameter(in="path", required=true, name="skpId", type="number", description="ID SKP"),
    *       @SWG\Parameter(
    *           in="body",
    *           name="body",
    *           description="Update SKP",
    *           required=true,
    *           @SWG\Schema(
    *               type="object",
    *               @SWG\Property(property="nama", type="string", description="Nama SKP"),
    *               @SWG\Property(property="nip", type="string", description="NIP (Only required when logged as admin or operator)"),
    *               @SWG\Property(property="kegiatan", type="number", description="ID Kegiatan"),
    *               @SWG\Property(property="tupoksi", type="number", description="ID Tupoksi"),
    *               @SWG\Property(property="waktu", type="number", description="Waktu yang diperlukan"),
    *               @SWG\Property(property="tipeWaktu", type="number", description="ID Tipe Waktu"),
    *               @SWG\Property(property="output", type="number", description="Banyaknya Output yang diharapkan"),
    *               @SWG\Property(property="tipeOutput", type="number", description="ID Tipe Output SKP"),
    *               @SWG\Property(property="biaya", type="number", description="ID Tipe Output SKP"),
    *               required={"nama", "waktu", "tipeWaktu", "output", "tipeOutput"}
    *           )
    *        ),
    *       security={"JWT": {}}
    *     )
    */
    public function update(Skp $skp, Request $req) {
        if(!$skp) {
            return $this->notFoundErrorMessage();
        }

        $user = JWTAuth::parseToken()->toUser();

        $validator = Validator::make($req->all(), [
            'nip' => $user->level <= 2 ? 'required' : '',
            'nama' => 'required',
            'waktu' => 'required',
            'tipeWaktu' => 'required',
            'output' => 'required',
            'tipeOutput' => 'required'
        ]);

        if($validator->fails()) {
            return $this->errorMessage($this->validatorMessage($validator->errors()));
        }

        if($req->input('kegiatan') && (!$req->input('biaya') || $req->input('biaya') <= 0)) {
            return $this->errorMessage('Biaya wajib diisi!');
        }

        try {
            $skp->nama = $req->input('nama');
            $skp->nip = $user->level <= 2 ? $req->input('nip') : $user->nip;
            $skp->tupoksi_id = $req->input('tupoksi');
            $skp->kegiatan_id = $req->input('kegiatan');
            $skp->waktu = $req->input('waktu');
            $skp->tipe_waktu_id = $req->input('tipeWaktu');
            $skp->output = $req->input('output');
            $skp->skp_output_id = $req->input('tipeOutput');
            $skp->biaya = $req->input('biaya');
            $skp->kualitas = 100;
            $skp->save();

            return $this->successMessage();
        } catch(\Exceptions $e) {
            return $this->errorMessage($e->getMessages(), 500);
        }        
    }

    /**
    * @SWG\Delete(
    *      path="/skp/{skpId}",
    *      operationId="hapusSkpId",
    *      tags={"Hapus"},
    *      summary="Hapus SKP",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Response(response=401, description="Tidak Berhak Untuk Menghapus/ Belum Login/ Unauthorized"),
    *       @SWG\Parameter(
    *           in="path",
    *           name="skpId",
    *           description="ID SKP",
    *           required=true,
    *           type="number"
    *       ),
    *       security={{
    *           "JWT": {}
    *       }}
    *     )
    */
    public function destroy(Skp $skp, Request $req) {
        if(!$skp) {
            return $this->notFoundErrorMessage();
        }

        $user = JWTAuth::parseToken()->toUser();

        if($user->level > 2 && $skp->nip != $user->nip) {
            return $this->errorMessage('Anda tidak berhak untuk menghapus SKP ini.', 401);
        }

        try {
            $skp->delete();
            return $this->successMessage();
        } catch(\Exceptions $e) {
            return $this->errorMessage($e->getMessages(), 500);
        }
    }
}
