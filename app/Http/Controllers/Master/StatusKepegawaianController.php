<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Master\StatusKepegawaian;
use Illuminate\Support\Facades\Validator;

class StatusKepegawaianController extends Controller
{
    private $validatorMessage =  [
        'nama.required' => 'Nama dibutuhkan.',
        'nama.unique' => 'Status yang sama telah ada di sistem.'
    ];


    /**
    * @SWG\Get(
    *      path="/master/status-kepegawaian/{statusKepegawaianId}",
    *      operationId="masterStatusKepegawaianDetail",
    *      tags={"Detail"},
    *      summary="Detail Status Kepegawaian",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           in="path",
    *           name="statusKepegawaianId",
    *           description="ID Status Kepegawaian",
    *           required=true,
    *           type="number"
    *       ),
    *     )
    */
    public function show(StatusKepegawaian $statusKepegawaian) {
        if(!$statusKepegawaian) {
            return $this->errorMessage('Data tidak ditemukan.', 404);
        }

        return $this->successMessageWithData($statusKepegawaian);
    }

    /**
    * @SWG\Get(
    *      path="/master/status-kepegawaian",
    *      operationId="masterSkpOutput",
    *      tags={"Daftar"},
    *      summary="Daftar Status Kepegawaian",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Mengurutkan data berdasarkan nama",
    *           in="query",
    *           name="sort",
    *           enum={"desc", "asc"},
    *       ),
    *       @SWG\Parameter(
    *           type="number",
    *           description="Membatasi banyaknya data yang ditampilkan",
    *           in="query",
    *           name="limit"
    *       ),
    *       @SWG\Parameter(
    *           type="number",
    *           description="Halaman",
    *           in="query",
    *           name="page"
    *       ),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Pencarian data berdasarkan nama",
    *           in="query",
    *           name="search"
    *       )
    *     )
    */
    public function index(Request $req) {
        $sort = $req->input('sort');
        $limit = $req->input('limit') ?? 10;
        $page = $req->input('page');
        $search = $req->input('search');

        $instance = StatusKepegawaian::take($limit)
            ->when(in_array($sort, $sortable), function($q) use($sort) {
                $q->orderBy('nama', $sort);
            })
            ->when(strlen($search), function($q) use($search) {
                $q->where('nama', 'like', "%{$search}%");
            })
        ;

        $total = $instance->count();

        $instance
            ->when($page > 0, function($q) use ($page) {
                if(is_numeric($page)) {
                    $skip = ($page - 1) * $limit;
                    $q->skip($skip);
                }
            })
        ;

        $result = $instance->get();

        return $this->successMessageWithData($result, $total);
    }

    /**
    * @SWG\Post(
    *      path="/master/status-kepegawaian",
    *      operationId="masterStatusKepegawaianTambah",
    *      tags={"Tambah"},
    *      summary="Tambah Status Kepegawaian",
    *      @SWG\Response(
    *          response=200,
    *          description="Berhasil Menambah Data"
    *       ),
    *       @SWG\Response(response=400, description="Input Salah, atau Kurang"),
    *       @SWG\Response(response=401, description="Pengguna Belum Login/Mendapatkan Token"),
    *       @SWG\Parameter(
    *           in="body",
    *           name="body",
    *           description="Tambah Satuan Kerja",
    *           required=true,
    *           @SWG\Schema(
    *               type="object",
    *               @SWG\Property(property="nama", type="string"),
    *               required={"nama"}
    *           )
    *        ),
    *       security={"JWT": {}}
    *     )
    */
    public function store(Request $req) {
        $validators = Validator::make($req->all(), [
            'nama' => 'required|unique:m_status_kepegawaian'
        ], $this->validatorMessage);

        if($validators->fails()) {
            return $this->errorMessage($this->validatorMessage($validators->errors()), 400);
        }

        $nama = $req->input('nama');

        try {
            $insert = new StatusKepegawaian();
            $insert->nama = $nama;
            $insert->save();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }

    public function update(StatusKepegawaian $statusKepegawaian, Request $req) {
        if(!$statusKepegawaian) {
            return $this->errorMessage('Data tidak ditemukan.', 404);
        }

        $validator = Validator::make($req->all(), [
            'nama' => 'required|unique:m_status_kepegawaian'
        ], $this->validatorMessage);

        if($validator->fails()) {
            return $this->errorMessage($this->validatorMessage($validator->errors()), 400);
        }

        try {
            $statusKepegawaian->nama = $req->input('nama');
            $statusKepegawaian->save();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }

    /**
    * @SWG\Delete(
    *      path="/master/status-kepegawaian/{statusKepegawaianId}",
    *      operationId="masterStatusKepegawaianHapus",
    *      tags={"Hapus"},
    *      summary="Hapus Status Kepegawaian",
    *      @SWG\Response(
    *          response=200,
    *          description="Berhasil Menghapus Data"
    *       ),
    *       @SWG\Response(response=404, description="Data Tidak Ditemukan"),
    *       @SWG\Parameter(
    *           in="path",
    *           name="statusKepegawaianId",
    *           description="ID Status Kepegawaian",
    *           required=true,
    *           type="number"
    *       ),
    *       security={"JWT": {}}
    *     )
    */
    public function destroy(StatusKepegawaian $statusKepegawaian) {
        if(!$statusKepegawaian) {
            return $this->errorMessage('Data tidak ditemukan.', 404);
        }

        try {
            $statusKepegawaian->delete();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 400);
        }
    }
}
