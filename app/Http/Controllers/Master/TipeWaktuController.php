<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Master\TipeWaktu;

class TipeWaktuController extends Controller
{
    /**
    * @SWG\Get(
    *      path="/master/tipe-waktu",
    *      operationId="masterTipeWaktu",
    *      tags={"Daftar"},
    *      summary="Daftar Tipe Waktu",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *     )
    */
    public function index(Request $req) {
        $result = TipeWaktu::all();
        return $this->successMessageWithData($result);
    }
}
