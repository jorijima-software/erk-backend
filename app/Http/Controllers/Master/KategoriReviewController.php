<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Master\KategoriReview;
use Illuminate\Support\Facades\Validator;

class KategoriReviewController extends Controller
{
    private $validatorMessage =  [
        'nama.required' => 'Nama dibutuhkan.',
        'nama.unique' => 'Nama kategori yang sama telah ada di sistem.'
    ];


    /**
    * @SWG\Get(
    *      path="/master/kategori-review/{kategoriReviewId}",
    *      operationId="masterKategoriReviewDetail",
    *      tags={"Detail"},
    *      summary="Detail Kategori Review",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           in="path",
    *           name="kategoriReviewId",
    *           description="ID Kategori Review",
    *           required=true,
    *           type="number"
    *       ),
    *     )
    */
    public function show(KategoriReview $kategoriReview) {
        if(!$kategoriReview) {
            return $this->errorMessage('Data tidak ditemukan.', 404);
        }

        return $this->successMessageWithData($agama);
    }

    /**
    * @SWG\Get(
    *      path="/master/kategori-review",
    *      operationId="masterKategoriReviewDaftar",
    *      tags={"Daftar"},
    *      summary="Daftar Kategori Review",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Mengurutkan data berdasarkan nama",
    *           in="query",
    *           name="sort",
    *           enum={"desc", "asc"},
    *       ),
    *       @SWG\Parameter(
    *           type="number",
    *           description="Membatasi banyaknya data yang ditampilkan",
    *           in="query",
    *           name="limit"
    *       ),
    *       @SWG\Parameter(
    *           type="number",
    *           description="Halaman",
    *           in="query",
    *           name="page"
    *       ),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Pencarian data berdasarkan nama",
    *           in="query",
    *           name="search"
    *       ),
    *     )
    */
    public function index(Request $req) {
        $sort = $req->input('sort');
        $limit = $req->input('limit') ?? 10;
        $page = $req->input('page');
        $search = $req->input('search');

        $instance = KategoriReview::take($limit)
            ->when(in_array($sort, $sortable), function($q) use($sort) {
                $q->orderBy('nama', $sort);
            })
            ->when(strlen($search), function($q) use ($search) {
                $q->where('nama', 'like', "%{$search}%");
            })
        ;

        $total = $instance->count();

        $instance
            ->when($page > 0, function($q) use ($page) {
                if(is_numeric($page)) {
                    $skip = ($page - 1) * $limit;
                    $q->skip($skip);
                }
            })
        ;

        $result = $instance->get();

        return $this->successMessageWithData($result, $total);
    }

    /**
    * @SWG\Post(
    *      path="/master/kategori-review",
    *      operationId="masterKategoriReview",
    *      tags={"Tambah"},
    *      summary="Tambah Kategori Review",
    *      @SWG\Response(
    *          response=200,
    *          description="Berhasil Menambah Data"
    *       ),
    *       @SWG\Response(response=400, description="Input Salah, atau Kurang"),
    *       @SWG\Response(response=401, description="Pengguna Belum Login/Mendapatkan Token"),
    *       @SWG\Parameter(
    *           in="body",
    *           name="body",
    *           description="Tambah Kategori Review",
    *           required=true,
    *           @SWG\Schema(
    *               type="object",
    *               @SWG\Property(property="nama", type="string"),
    *               required={"nama", "eselon"}
    *           )
    *        ),
    *       security={"JWT": {}}
    *     )
    */
    public function store(Request $req) {
        $validators = Validator::make($req->all(), [
            'nama' => 'required|unique:m_kategori_review'
        ], $this->validatorMessage);

        if($validators->fails()) {
            return $this->errorMessage($this->validatorMessage($validators->errors()), 400);
        }

        try {
            $insert = new KategoriReview();
            $insert->nama = $req->input('nama');
            $insert->save();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }

    /**
    * @SWG\Patch(
    *      path="/master/kategori-review/{kategoriReviewId}",
    *      operationId="masterKategoriReviewUpdate",
    *      tags={"Update"},
    *      summary="Tambah Kategori Review",
    *      @SWG\Response(
    *          response=200,
    *          description="Berhasil Mengubah Data"
    *       ),
    *       @SWG\Response(response=400, description="Input Salah, atau Kurang"),
    *       @SWG\Response(response=401, description="Pengguna Belum Login/Mendapatkan Token"),
    *       @SWG\Parameter(in="path", name="kategoriReviewId", required=true, type="number", description="ID Kategori Review"),
    *       @SWG\Parameter(
    *           in="body",
    *           name="body",
    *           description="Update Kategori Review",
    *           required=true,
    *           @SWG\Schema(
    *               type="object",
    *               @SWG\Property(property="nama", type="string"),
    *               required={"nama", "eselon"}
    *           )
    *        ),
    *       security={"JWT": {}}
    *     )
    */
    public function update(KategoriReview $kategoriReview, Request $req) {
        if(!$kategoriReview) {
            return $this->errorMessage('Data tidak ditemukan.', 404);
        }

        $validator = Validator::make($req->all(), [
            'nama' => 'required|unique:m_kategori_review'
        ], $this->validatorMessage);

        if($validator->fails()) {
            return $this->errorMessage($this->validatorMessage($validator->errors()), 400);
        }

        try {
            $kategoriReview->nama = $req->input('nama');
            $kategoriReview->save();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }

    /**
    * @SWG\Delete(
    *      path="/master/kategori-review/{kategoriReviewId}",
    *      operationId="masterKategoriReviewHapus",
    *      tags={"Hapus"},
    *      summary="Hapus Kategori Review",
    *      @SWG\Response(
    *          response=200,
    *          description="Berhasil Menghapus Data"
    *       ),
    *       @SWG\Response(response=404, description="Data Tidak Ditemukan"),
    *       @SWG\Parameter(
    *           in="path",
    *           name="kategoriReviewId",
    *           description="ID Kategori Review",
    *           required=true,
    *           type="number"
    *       ),
    *       security={"JWT": {}}
    *     )
    */
    public function destroy(KategoriReview $kategoriReview) {
        if(!$kategoriReview) {
            return $this->errorMessage('Data tidak ditemukan.', 404);
        }

        try {
            $kategoriReview->delete();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 400);
        }
    }
}
