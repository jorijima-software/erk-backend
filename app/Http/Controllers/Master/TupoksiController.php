<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Master\Tupoksi;
use App\Model\Pegawai;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\Master\TupoksiResource;
use Tymon\JWTAuth\Facades\JWTAuth;

class TupoksiController extends Controller
{
    protected $validatorMessages = [
        'nama.required' => 'Nama dibutuhkan.',
        'jabatan.required' => 'Jabatan dibutuhkan.',
        'kode.required' => 'Kode dibutuhkan.',
        'kode.unique' => 'Terdapat tupoksi dengan kode yang sama di dalam sistem.',
    ];

    /**
    * @SWG\Get(
    *      path="/master/tupoksi/{tupoksiId}",
    *      operationId="tupoksiId",
    *      tags={"Detail"},
    *      summary="Detail Tupoksi (Tugas Pokok dan Fungsi)",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           in="path",
    *           name="tupoksiId",
    *           description="ID Tupoksi",
    *           required=true,
    *           type="number"
    *       ),
    *     )
    */
    public function show(Tupoksi $tupoksi) {
        if(!$tupoksi) {
            return $this->notFoundErrorMessage();
        }

        return $this->successMessageWithData($tupoksi);
    }

    /**
    * @SWG\Get(
    *      path="/master/tupoksi",
    *      operationId="masterTupoksiDaftar",
    *      tags={"Daftar"},
    *      summary="Daftar Tupoksi (Tugas Pokok dan Fungsi)",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Mengurutkan data berdasarkan nama",
    *           in="query",
    *           name="sort",
    *           enum={"desc", "asc"},
    *       ),
    *       @SWG\Parameter(
    *           type="number",
    *           description="Membatasi banyaknya data yang ditampilkan",
    *           in="query",
    *           name="limit"
    *       ),
    *       @SWG\Parameter(
    *           type="number",
    *           description="Halaman",
    *           in="query",
    *           name="page"
    *       ),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Pencarian data berdasarkan nama",
    *           in="query",
    *           name="search"
    *       ),
    *       @SWG\Parameter(
    *           type="number",
    *           description="Jabatan",
    *           in="query",
    *           name="jabatan"
    *       ),
    *     )
    */
    public function index(Request $req) {
        $user = JWTAuth::parseToken()->toUser();
        $pegawai = Pegawai::where('nip', $user->nip)->first();

        $sort = $req->input('sort');
        $limit = $req->input('limit') ?? 10;
        $page = $req->input('page');
        $search = $req->input('search');
        $jabatan = $req->input('jabatan');

        $instance = Tupoksi::when(in_array($sort, $this->sortable), function($q) use ($sort) {
                $q->orderBy('nama', $sort);
            })
            // ->when($user->level <= 2, function($q) use ($jabatan) {
            //     $q->where('jabatan_id', $jabatan);
            // })
            ->when($user->level > 2, function($q) use ($pegawai) {
                $q->where('jabatan_id', $pegawai->jabatan_id);
            })
            ->take($limit)
            ->when(strlen($search) > 0, function($q) use ($search) {
                $q->where('nama', 'like', "%{$search}%");
            })
        ;

        $total = $instance->count();

        $instance->when($page > 0, function($q) use ($page, $limit) {
            $skip = ($page - 1) * $limit;
            $q->skip($skip);
        });

        $result = TupoksiResource::collection($instance->get());

        return $this->successMessageWithData($result, $total);
    }

    /**
    * @SWG\Post(
    *      path="/master/tupoksi",
    *      operationId="masterTupoksiTambah",
    *      tags={"Tambah"},
    *      summary="Tambah Tugas Pokok dan Fungsi (Tupoksi)",
    *      @SWG\Response(
    *          response=200,
    *          description="Berhasil Menambah Data"
    *       ),
    *       @SWG\Response(response=400, description="Input Salah, atau Kurang"),
    *       @SWG\Response(response=401, description="Pengguna Belum Login/Mendapatkan Token"),
    *       @SWG\Parameter(
    *           in="body",
    *           name="body",
    *           description="Tambah Tupoksi",
    *           required=true,
    *           @SWG\Schema(
    *               type="object",
    *               @SWG\Property(property="nama", type="string"),
    *               @SWG\Property(property="kode", type="string", description="Kode Tupoksi"),
    *               @SWG\Property(property="jabatan", type="number", description="ID Jabatan"),
    *               @SWG\Property(property="utama", type="boolean", description="Apakah ini termasuk tupoksi utama?"),
    *               required={"nama", "jabatan", "kode"}
    *           )
    *        ),
    *       security={"JWT": {}}
    *     )
    */
    public function store(Request $req) {
        $validator = Validator::make($req->all(), [
            'nama' => 'required',
            'jabatan' => 'required',
            'kode' => 'required|unique:m_tupoksi'
        ], $this->validatorMessages);

        if($validator->fails()) {
            return $this->errorMessage($this->validatorMessage($validator->errors()));
        }

        try {
            $insert = new Tupoksi();
            $insert->nama = $req->input('nama');
            $insert->jabatan_id = $req->input('jabatan');
            $insert->kode = $req->input('kode');
            $insert->utama = $req->input('utama') ?? true;
            $insert->save();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }

    public function update(Tupoksi $tupoksi, Request $req) {
        if(!$tupoksi) {
            return $this->notFoundErrorMessage();
        }

        $validator = Validator::make($req->all(), [
            'nama' => 'required',
            'jabatan' => 'required',
            'kode' => 'required|unique:m_tupoksi'
        ], $this->validatorMessages);

        if($validator->fails()) {
            return $this->errorMessage($this->validatorMessage($validator->errors()));
        }

        try {
            $tupoksi->nama = $req->input('nama');
            $tupoksi->jabatan_id = $req->input('jabatan_id');
            $tupoksi->kode = $req->input('kode');
            $tupoksi->utama = $req->input('utama');
            $tupoksi->save();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }

    /**
    * @SWG\Delete(
    *      path="/master/tupoksi/{tupoksiId}",
    *      operationId="masterTupoksiHapus",
    *      tags={"Hapus"},
    *      summary="Hapus Tupoksi",
    *      @SWG\Response(
    *          response=200,
    *          description="Berhasil Menghapus Data"
    *       ),
    *       @SWG\Response(response=404, description="Data Tidak Ditemukan"),
    *       @SWG\Parameter(
    *           in="path",
    *           name="tupoksiId",
    *           description="ID Tupoksi",
    *           required=true,
    *           type="number"
    *       ),
    *       security={"JWT": {}}
    *     )
    */
    public function destroy(Tupoksi $tupoksi) {
        if(!$tupoksi) {
            return $this->notFoundErrorMessage();
        }

        try {
            $tupoksi->delete();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }
}
