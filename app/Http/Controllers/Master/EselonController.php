<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Master\Eselon;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\Master\EselonResource;

class EselonController extends Controller
{
    private $validatorMessage = [
        'nama.required' => 'Nama dibutuhkan.',
        'kode.required' => 'Kode eselon dibutuhkan.',
        'kode.unique' => 'Kode eselon yang sama telah terdaftar.',
        'golonganAwal.required' => 'Golongan awal dibutuhkan.',
        'golonganAkhir.required' => 'Golongan akhir dibutuhkan.',
    ];


    /**
    * @SWG\Get(
    *      path="/master/eselon/{eselonId}",
    *      operationId="masterEselonDetail",
    *      tags={"Detail"},
    *      summary="Detail Eselon",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           in="path",
    *           name="eselonId",
    *           description="ID Eselon",
    *           required=true,
    *           type="number"
    *       ),
    *     )
    */
    public function show(Eselon $eselon) {
        if(!$eselon) {
            return $this->errorMessage('Data tidak ditemukan.', 404);
        }

        return $this->successMessageWithData($eselon);
    }

    /**
    * @SWG\Get(
    *      path="/master/eselon",
    *      operationId="masterEselonDaftar",
    *      tags={"Daftar"},
    *      summary="Daftar Eselon",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Mengurutkan data berdasarkan nama",
    *           in="query",
    *           name="sort",
    *           enum={"desc", "asc"},
    *       ),
    *       @SWG\Parameter(
    *           type="number",
    *           description="Membatasi banyaknya data yang ditampilkan",
    *           in="query",
    *           name="limit"
    *       ),
    *       @SWG\Parameter(
    *           type="number",
    *           description="Halaman",
    *           in="query",
    *           name="page"
    *       ),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Pencarian data berdasarkan nama",
    *           in="query",
    *           name="search"
    *       ),
    *     )
    */
    public function index(Request $req) {
        $sort = $req->input('sort');
        $limit = $req->input('limit') ?? 10;
        $page = $req->input('page');
        $search = $req->input('search');

        $instance = Eselon::take($limit)
            ->when(in_array($sort, $this->sortable), function($q) use($sort) {
                $q->orderBy('nama', $sort);
            })
            ->when(strlen($search) > 0, function($q) use($search) {
                $q->where('nama', 'like', "%{$search}%");
            })
            ;

        $total = $instance->count();
        $instance
            ->when($page > 0, function($q) use ($page, $limit) {
                $skip = ($page - 1) * $limit;
                $q->skip($skip);
            });

        $result = EselonResource::collection($instance->get());

        return $this->successMessageWithData($result, $total);
    }

    /**
    * @SWG\Post(
    *      path="/master/eselon",
    *      operationId="masterEselonTambah",
    *      tags={"Tambah"},
    *      summary="Tambah Eselon",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           in="body",
    *           name="body",
    *           description="",
    *           required=true,
    *           @SWG\Schema(
    *               type="object",
    *               @SWG\Property(property="nama", type="string"),
    *               @SWG\Property(property="kode", type="number"),
    *               @SWG\Property(property="golonganAwal", type="number"),
    *               @SWG\Property(property="golonganAkhir", type="number"),
    *               required={"nama", "kode", "golonganAwal", "golonganAkhir"}
    *           )
    *        ),
    *       security={"JWT": {}}
    *     )
    */
    public function store(Request $req) {
        $validator = Validator::make($req->all(), [
            'nama' => 'required',
            'kode' => 'required|unique:m_eselon',
            'golonganAwal' => 'required',
            'golonganAkhir' => 'required'
        ], $this->validatorMessage);

        if($validator->fails()) {
            return $this->errorMessage($this->validatorMessage($validator->errors()));
        }

        try {
            $insert = new Eselon();
            $insert->nama = $req->input('nama');
            $insert->kode = $req->input('kode');
            $insert->golongan_awal_id = $req->input('golonganAwal');
            $insert->golongan_akhir_id = $req->input('golonganAkhir');
            $insert->save();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }

    /**
    * @SWG\Patch(
    *      path="/master/eselon/{eselonId}",
    *      operationId="masterEselonUpdate",
    *      tags={"Update"},
    *      summary="Update Agama",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           in="body",
    *           name="eselonId",
    *           description="ID Eselon",
    *           required=true,
    *           @SWG\Schema(
    *               type="object",
    *               @SWG\Property(property="nama", type="string"),
    *               @SWG\Property(property="kode", type="number"),
    *               @SWG\Property(property="golonganAwal", type="number"),
    *               @SWG\Property(property="golonganAkhir", type="number"),
    *           )
    *        ),
    *       security={"JWT": {}}
    *     )
    */
    public function update(Eselon $eselon, Request $req) {
        if(!$eselon) {
            return $this->errorMessage('Data tidak ditemukan.', 404);
        }
        $validator = Validator::make($req->all(), [
            'nama' => 'required',
            'kode' => 'required|unique:m_eselon',
            'golonganAwal' => 'required',
            'golonganAkhir' => 'required'
        ], $this->validatorMessage);

        if($validator->fails()) {
            return $this->errorMessage($this->validatorMessage($validator->errors()));
        }

        try {
            $eselon->nama = $req->input('nama');
            $eselon->kode = $req->input('kode');
            $eselon->golongan_awal_id = $req->input('golonganAwal');
            $eselon->golongan_akhir_id = $req->input('golonganAkhir');
            $eselon->save();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }

    /**
    * @SWG\Delete(
    *      path="/master/eselon/{eselonId}",
    *      operationId="masterEselonDelete",
    *      tags={"Hapus"},
    *      summary="Hapus Eselon",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           in="path",
    *           name="eselonId",
    *           description="ID Eselon",
    *           required=true,
    *           type="number"
    *       ),
    *       security={"JWT": {}}
    *     )
    */
    public function destroy(Golongan $golongan) {
        if(!$golongan) {
            return $this->notFoundErrorMessage();
        }

        try {
            $golongan->delete();
            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }
}
