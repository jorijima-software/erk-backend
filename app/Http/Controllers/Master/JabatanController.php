<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Master\Jabatan;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\Master\JabatanResource;

class JabatanController extends Controller
{
    protected $validatorMessage = [
        'nama.required' => 'Nama dibutuhkan.',
        'eselon.required' => 'Eselon dibutuhkan.',
        'kode.required' => 'Kode dibutuhkan.'
    ];


    /**
    * @SWG\Get(
    *      path="/master/jabatan/{jabatanId}",
    *      operationId="masterJabatanDetail",
    *      tags={"Detail"},
    *      summary="Detail Jabatan",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           in="path",
    *           name="jabatanId",
    *           description="ID Jabatan",
    *           required=true,
    *           type="number"
    *       ),
    *     )
    */
    public function show(Jabatan $jabatan) {
        if(!$jabatan) {
            return $this->notFoundErrorMessage();
        }

        return $this->successMessageWithData($jabatan);
    }

    /**
    * @SWG\Get(
    *      path="/master/jabatan",
    *      operationId="masterJabatanDaftar",
    *      tags={"Daftar"},
    *      summary="Daftar Jabatan",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Mengurutkan data berdasarkan nama",
    *           in="query",
    *           name="sort",
    *           enum={"desc", "asc"},
    *       ),
    *       @SWG\Parameter(
    *           type="number",
    *           description="Membatasi banyaknya data yang ditampilkan",
    *           in="query",
    *           name="limit"
    *       ),
    *       @SWG\Parameter(
    *           type="number",
    *           description="Halaman",
    *           in="query",
    *           name="page"
    *       ),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Pencarian data berdasarkan nama",
    *           in="query",
    *           name="search"
    *       ),
    *     )
    */
    public function index(Request $req) {
        $sort = $req->input('sort');
        $limit = $req->input('limit') ?? 10;
        $page = $req->input('page');
        $search = $req->input('search');

        $instance = Jabatan::when(in_array($sort, $this->sortable), function($q) use ($sort) {
                $q->orderBy('nama', $sort);
            })
            ->when(strlen($search) > 0, function($q) use ($search) {
                $q->where('nama', 'like', "%{$search}%");
            })
            ->take($limit)
        ;

        $total = $instance->count();

        $instance->when($page > 0, function($q) use ($page, $limit) {
            $skip = ($page - 1) * $limit;
        });

        $result = JabatanResource::collection($instance->get());

        return $this->successMessageWithData($result, $total);
    }

    /**
    * @SWG\Post(
    *      path="/master/jabatan",
    *      operationId="masterJabatanTambah",
    *      tags={"Tambah"},
    *      summary="Tambah Jabatan",
    *      @SWG\Response(
    *          response=200,
    *          description="Berhasil Menambah Data"
    *       ),
    *       @SWG\Response(response=400, description="Input Salah, atau Kurang"),
    *       @SWG\Response(response=401, description="Pengguna Belum Login/Mendapatkan Token"),
    *       @SWG\Parameter(
    *           in="body",
    *           name="body",
    *           description="Tambah Jabatan",
    *           required=true,
    *           @SWG\Schema(
    *               type="object",
    *               @SWG\Property(property="nama", type="string"),
    *               @SWG\Property(property="kode", type="string"),
    *               @SWG\Property(property="eselon", type="number"),
    *               @SWG\Property(property="jenis", type="number"),
    *               @SWG\Property(property="jabatanParent", type="string", description="Kode Jabatan Atasan"),
    *               required={"nama", "eselon", "kode"}
    *           )
    *        ),
    *       security={"JWT": {}}
    *     )
    */
    public function store(Request $req) {
        $validator = Validator::make($req->all(), [
            'nama' => 'required',
            'eselon' => 'required',
            'kode' => 'required',
        ], $this->validatorMessage);

        if($validator->fails()) {
            return $this->errorMessage($this->validatorMessage($validator->errors()));
        }

        try {
            $insert = new Jabatan();
            $insert->nama       = $req->input('nama');
            $insert->eselon_id  = $req->input('eselon');
            $insert->kode       = $req->input('kode');            
            $insert->jenis_id   = $req->input('jenis') ?? 1;
            $insert->kode_parent = $req->input('jabatanParent') ?? null;
            $insert->save();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }

    /**
    * @SWG\Patch(
    *      path="/master/jabatan/{jabatanId}",
    *      operationId="masterJabatanUpdate",
    *      tags={"Update"},
    *      summary="Tambah Jabatan",
    *      @SWG\Response(
    *          response=200,
    *          description="Berhasil Menambah Data"
    *       ),
    *       @SWG\Response(response=400, description="Input Salah, atau Kurang"),
    *       @SWG\Response(response=401, description="Pengguna Belum Login/Mendapatkan Token"),
    *       @SWG\Parameter(
    *           in="path",
    *           name="jabatanId",
    *           required=true,
    *           description="ID Jabatan",
    *           type="number"
    *       ),
    *       @SWG\Parameter(
    *           in="body",
    *           name="body",
    *           description="Tambah Jabatan",
    *           required=true,
    *           @SWG\Schema(
    *               type="object",
    *               @SWG\Property(property="nama", type="string"),
    *               @SWG\Property(property="kode", type="string"),
    *               @SWG\Property(property="eselon", type="number"),
    *               @SWG\Property(property="jenis", type="number"),
    *               @SWG\Property(property="jabatanParent", type="string", description="Kode Jabatan Atasan"),
    *               required={"nama", "eselon", "kode"}
    *           )
    *        ),
    *       security={"JWT": {}}
    *     )
    */
    public function update(Jabatan $jabatan, Request $req) {
        if(!$jabatan) {
            return $this->notFoundErrorMessage();
        }

        $validator = Validator::make($req->all(), [
            'nama' => 'required',
            'eselon' => 'required',
            'kode' => 'required',            
        ], $this->validatorMessage);

        if($validator->fails()) {
            return $this->errorMessage($this->validatorMessage($validator->errors()));
        }

        try {
            $jabatan->nama = $req->input('nama');
            $jabatan->kode = $req->input('kode');
            $jabatan->eselon_id = $req->input('eselon');
            $jabatan->jenis = $req->input('jenis') ?? 1;
            $jabatan->jabatan_parent_id = $req->input('jabatanParent') ?? null;
            $jabatan->save();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }

    /**
    * @SWG\Delete(
    *      path="/master/jabatan/{jabatanId}",
    *      operationId="masterJabatanDelete",
    *      tags={"Hapus"},
    *      summary="Hapus Jabatan",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           in="path",
    *           name="jabatanId",
    *           description="ID Jabatan",
    *           required=true,
    *           type="number"
    *       ),
    *       security={{
    *           "JWT": {}
    *       }}
    *     )
    */
    public function destroy(Jabatan $jabatan) {
        if(!$jabatan) {
            return $this->notFoundErrorMessage();
        }

        try {
            $jabatan->delete();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }
}
