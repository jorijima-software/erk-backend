<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Master\JenisJabatan;
use Illuminate\Support\Facades\Validator;

class JenisJabatanController extends Controller
{

    /**
    * @SWG\Get(
    *      path="/master/jenis-jabatan/{jenisJabatanId}",
    *      operationId="masterJenisJabatan",
    *      tags={"Detail"},
    *      summary="Detail Jenis Jabatan",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           in="path",
    *           name="jenisJabatanId",
    *           description="ID Jenis Jabatan",
    *           required=true,
    *           type="number"
    *       ),
    *     )
    */
    public function show(JenisJabatan $jenisJabatan) {
        if(!$jenisJabatan) {
            return $this->notFoundErrorMessage();
        }

        return $this->successMessageWithData($jenisJabatan);
    }

    /**
    * @SWG\Get(
    *      path="/master/jenis-jabatan",
    *      operationId="masterJenisJabatanDaftar",
    *      tags={"Daftar"},
    *      summary="Daftar Jenis Jabatan",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Mengurutkan data berdasarkan nama",
    *           in="query",
    *           name="sort",
    *           enum={"desc", "asc"},
    *       ),
    *       @SWG\Parameter(
    *           type="number",
    *           description="Membatasi banyaknya data yang ditampilkan",
    *           in="query",
    *           name="limit"
    *       ),
    *       @SWG\Parameter(
    *           type="number",
    *           description="Halaman",
    *           in="query",
    *           name="page"
    *       ),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Pencarian data berdasarkan nama",
    *           in="query",
    *           name="search"
    *       ),
    *     )
    */
    public function index(Request $req) {
        $sort = $req->input('sort');
        $limit = $req->input('limit') ?? 10;
        $page = $req->input('page');
        $search = $req->input('search');

        $instance = JenisJabatan::when(in_array($sort, $this->sortable), function($q) use ($sort) {
                $q->orderBy('nama', $sort);
            })
            ->when(strlen($search) > 0, function($q) use($search) {
                $q->where('nama', 'like', "%{$search}%");
            })
            ->take($limit)
        ;

        $total = $instance->count();

        $instance->when($page > 0, function($q) use ($page, $limit) {
            $skip = ($page - 1) * $limit;
            $q->skip($skip);
        });

        $result = $instance->get();

        return $this->successMessageWithData($result, $total);
    }

    /**
    * @SWG\Post(
    *      path="/master/jenis-jabatan",
    *      operationId="masterJenisJabatanTambah",
    *      tags={"Tambah"},
    *      summary="Tambah Jenis Jabatan",
    *      @SWG\Response(
    *          response=200,
    *          description="Berhasil Menambah Data"
    *       ),
    *       @SWG\Response(response=400, description="Input Salah, atau Kurang"),
    *       @SWG\Response(response=401, description="Pengguna Belum Login/Mendapatkan Token"),
    *       @SWG\Parameter(
    *           in="body",
    *           name="body",
    *           description="Tambah Jenis Jabatan",
    *           required=true,
    *           @SWG\Schema(
    *               type="object",
    *               @SWG\Property(property="nama", type="string"),
    *               required={"nama"}
    *           )
    *        ),
    *       security={"JWT": {}}
    *     )
    */
    public function store(Request $req) {
        $validator = Validator::make($req->all(), [
            'nama' => 'required',
        ], $this->validatorMessages);

        if($validator->fails()) {
            return $this->errorMessage($this->validatorMessage($validator->errors()));
        }

        try {
            $insert = new JenisJabatan();
            $insert->nama = $req->input('nama');
            $insert->save();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }

    /**
    * @SWG\Patch(
    *      path="/master/jenis-jabatan/{jenisJabatanId}",
    *      operationId="masterJenisJabatanUpdate",
    *      tags={"Update"},
    *      summary="Update Jenis Jabatan",
    *      @SWG\Response(
    *          response=200,
    *          description="Berhasil Menambah Data"
    *       ),
    *       @SWG\Response(response=400, description="Input Salah, atau Kurang"),
    *       @SWG\Response(response=401, description="Pengguna Belum Login/Mendapatkan Token"),
    *       @SWG\Parameter(in="path", name="jenisJabatanId", required=true, description="ID Jenis Jabatan", type="number"),
    *       @SWG\Parameter(
    *           in="body",
    *           name="body",
    *           description="Tambah Jenis Jabatan",
    *           required=true,
    *           @SWG\Schema(
    *               type="object",
    *               @SWG\Property(property="nama", type="string"),
    *               required={"nama"}
    *           )
    *        ),
    *       security={"JWT": {}}
    *     )
    */
    public function update(JenisJabatan $jenisJabatan, Request $req) {
        if(!$jenisJabatan) {
            return $this->notFoundErrorMessage();
        }

        $validator = Validator::make($req->all(), [
            'nama' => 'required|unique:m_jenis_jabatan',
        ], $this->validatorMessages);

        if($validator->fails()) {
            return $this->errorMessage($this->validatorMessage($validator->errors()));
        }

        try {
            $jenisJabatan->nama = $req->input('nama');
            $jenisJabatan->save();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }

    /**
    * @SWG\Delete(
    *      path="/master/jenis-jabatan/{jenisJabatanId}",
    *      operationId="jenisJabatanHapus",
    *      tags={"Hapus"},
    *      summary="Hapus Jenis Jabatan",
    *      @SWG\Response(
    *          response=200,
    *          description="Berhasil Menghapus Data"
    *       ),
    *       @SWG\Response(response=404, description="Data Tidak Ditemukan"),
    *       @SWG\Parameter(
    *           in="path",
    *           name="jenisJabatanId",
    *           description="ID Jenis Jabatan",
    *           required=true,
    *           type="number"
    *       ),
    *       security={"JWT": {}}
    *     )
    */
    public function destroy(JenisJabatan $jenisJabatan) {
        if(!$jenisJabatan) {
            return $this->notFoundErrorMessage();
        }

        try {
            $jenisJabatan->delete();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }
}
