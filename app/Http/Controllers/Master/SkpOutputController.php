<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Master\SkpOutput;
use Illuminate\Support\Facades\Validator;

class SkpOutputController extends Controller
{

    /**
    * @SWG\Get(
    *      path="/master/skp-output/{skpOutputId}",
    *      operationId="masterSkpOutputId",
    *      tags={"Detail"},
    *      summary="Detail Sasaran Kerja Pegawai",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           in="path",
    *           name="skpOutputId",
    *           description="ID SKP Output",
    *           required=true,
    *           type="number"
    *       ),
    *     )
    */
    public function show(SkpOutput $skpOutput) {
        if(!$skpOutput) {
            return $this->notFoundErrorMessage();
        }

        return $this->successMessageWithData($skpOutput);
    }

    /**
    * @SWG\Get(
    *      path="/master/skp-output",
    *      operationId="masterSkpOutput",
    *      tags={"Daftar"},
    *      summary="Daftar Tipe Output Sasaran Kerja Pegawai",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Mengurutkan data berdasarkan nama",
    *           in="query",
    *           name="sort",
    *           enum={"desc", "asc"},
    *       ),
    *       @SWG\Parameter(
    *           type="number",
    *           description="Membatasi banyaknya data yang ditampilkan",
    *           in="query",
    *           name="limit"
    *       ),
    *       @SWG\Parameter(
    *           type="number",
    *           description="Halaman",
    *           in="query",
    *           name="page"
    *       ),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Pencarian data berdasarkan nama",
    *           in="query",
    *           name="search"
    *       )
    *     )
    */
    public function index(Request $req) {
        $sort = $req->input('sort');
        $limit = $req->input('limit') ?? 10;
        $page = $req->input('page');
        $search = $req->input('search');

        $instance = SkpOutput::when(in_array($sort, $this->sortable), function($q) use ($sort) {
                $q->orderBy('nama', $sort);
            })
            ->take($limit)
            ->when(strlen($search), function($q) use ($search) {
                $q->where('nama', 'like', "%{$search}%");
            });
        ;

        $total = $instance->count();

        $instance->when($page > 0, function($q) use($page, $limit) {
            $skip = ($page - 1) * $limit;
            $q->skip($skip);
        });

        $result = $instance->get();

        return $this->successMessageWithData($result, $total);
    }

    /**
    * @SWG\Post(
    *      path="/master/skp-output",
    *      operationId="masterSkpOutputTambah",
    *      tags={"Tambah"},
    *      summary="Tambah Tipe Output Sasaran Kerja Pegawai",
    *      @SWG\Response(
    *          response=200,
    *          description="Berhasil Menambah Data"
    *       ),
    *       @SWG\Response(response=400, description="Input Salah, atau Kurang"),
    *       @SWG\Response(response=401, description="Pengguna Belum Login/Mendapatkan Token"),
    *       @SWG\Parameter(
    *           in="body",
    *           name="body",
    *           description="Tambah Tipe Output SKP",
    *           required=true,
    *           @SWG\Schema(
    *               type="object",
    *               @SWG\Property(property="nama", type="string"),
    *               required={"nama"}
    *           )
    *        ),
    *       security={"JWT": {}}
    *     )
    */
    public function store(Request $req) {
        $validator = Validator::make($req->all(), [
            'nama' => 'required|unique:m_skp_output'
        ], $this->validatorMessages);

        if($validator->fails()) {
            return $this->errorMessage($this->validatorMessage($validator->errors()));
        }

        try {
            $insert = new SkpOutput();
            $insert->nama = $req->input('nama');
            $insert->save();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }

    public function update(SkpOutput $skpOutput, Request $req) {
        if(!$skpOutput) {
            return $this->notFoundErrorMessage();
        }

        $validator = Validator::make($req->all(), [
            'nama' => 'required|unique:m_skp_output'
        ], $this->validatorMessages);

        if($validator->fails()) {
            return $this->errorMessage($this->validatorMessage($validator->errors()));
        }

        try {
            $skpOutput->nama = $req->input('nama');
            $skpOutput->save();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }

    /**
    * @SWG\Delete(
    *      path="/master/skp-output/{skpOutputId}",
    *      operationId="masterSKPOutputHapus",
    *      tags={"Hapus"},
    *      summary="Hapus Tipe Output SKP",
    *      @SWG\Response(
    *          response=200,
    *          description="Berhasil Menghapus Data"
    *       ),
    *       @SWG\Response(response=404, description="Data Tidak Ditemukan"),
    *       @SWG\Parameter(
    *           in="path",
    *           name="skpOutputId",
    *           description="ID Tipe SKP Output",
    *           required=true,
    *           type="number"
    *       ),
    *       security={"JWT": {}}
    *     )
    */
    public function destroy(SkpOutput $skpOutput) {
        if(!$skpOutput) {
            return $this->notFoundErrorMessage();
        }

        try {
            $skpOutput->delete();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }
}
