<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Master\Kegiatan;
use Illuminate\Support\Facades\Validator;

class KegiatanController extends Controller
{
    private $validatorMessage =  [
        'nama.required' => 'Nama dibutuhkan.',
        'nama.unique' => 'Nama kegiatan yang sama telah ada di sistem.',
        'biaya.required' => 'Biaya dibutuhkan.'
    ];


    /**
    * @SWG\Get(
    *      path="/master/kegiatan/{kegiatanId}",
    *      operationId="masterKegiatanDetail",
    *      tags={"Detail"},
    *      summary="Detail Kategori Review",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           in="path",
    *           name="kegiatanId",
    *           description="ID Kegiatan",
    *           required=true,
    *           type="number"
    *       ),
    *     )
    */
    public function show(Kegiatan $kegiatan) {
        if(!$kegiatan) {
            return $this->errorMessage('Data tidak ditemukan.', 404);
        }

        return $this->successMessageWithData($kegiatan);
    }


    /**
    * @SWG\Get(
    *      path="/master/kegiatan",
    *      operationId="masterKegiatanDaftar",
    *      tags={"Daftar"},
    *      summary="Daftar Kegiatan",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Mengurutkan data berdasarkan nama",
    *           in="query",
    *           name="sort",
    *           enum={"desc", "asc"},
    *       ),
    *       @SWG\Parameter(
    *           type="number",
    *           description="Membatasi banyaknya data yang ditampilkan",
    *           in="query",
    *           name="limit"
    *       ),
    *       @SWG\Parameter(
    *           type="number",
    *           description="Halaman",
    *           in="query",
    *           name="page"
    *       ),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Pencarian data berdasarkan nama",
    *           in="query",
    *           name="search"
    *       ),
    *     )
    */
    public function index(Request $req) {
        $sort = $req->input('sort');
        $limit = $req->input('limit') ?? 10;
        $page = $req->input('page');
        $search = $req->input('search');

        $instance = Kegiatan::take($limit)
            ->when(in_array($sort, $this->sortable), function($q) use($sort) {
                $q->orderBy('nama', $sort);
            })
            ->when(strlen($search) > 0, function($q) use($search) {
                $q->where('nama', 'like', "%{$search}%");
            })
        ;

        $total = $instance->count();

        $instance
            ->when($page > 0, function($q) use ($page, $limit) {
                if(is_numeric($page)) {
                    $skip = ($page - 1) * $limit;
                    $q->skip($skip);
                }
            })
        ;

        $result = $instance->get();

        return $this->successMessageWithData($result, $total);
    }

    /**
    * @SWG\Post(
    *      path="/master/kegiatan",
    *      operationId="masterKegiatanTambah",
    *      tags={"Tambah"},
    *      summary="Tambah Kegiatan",
    *      @SWG\Response(
    *          response=200,
    *          description="Berhasil Menambah Data"
    *       ),
    *       @SWG\Response(response=400, description="Input Salah, atau Kurang"),
    *       @SWG\Response(response=401, description="Pengguna Belum Login/Mendapatkan Token"),
    *       @SWG\Parameter(
    *           in="body",
    *           name="body",
    *           description="Tambah Kegiatan",
    *           required=true,
    *           @SWG\Schema(
    *               type="object",
    *               @SWG\Property(property="nama", type="string"),
    *               @SWG\Property(property="biaya", type="number"),
    *               @SWG\Property(property="kode", type="string"),
    *               required={"nama", "biaya"}
    *           )
    *        ),
    *       security={"JWT": {}}
    *     )
    */
    public function store(Request $req) {
        $validators = Validator::make($req->all(), [
            'nama' => 'required|unique:m_kegiatan',
            'biaya' => 'required',
            'kode' => 'required|unique:m_kegiatan'
        ], $this->validatorMessage);

        if($validators->fails()) {
            return $this->errorMessage($this->validatorMessage($validators->errors()), 400);
        }

        try {
            $insert = new Kegiatan();
            $insert->nama = $req->input('nama');
            $insert->biaya = $req->input('biaya');
            $insert->kode = $req->input('kode');
            $insert->utama = $req->input('utama') ?? true;
            $insert->tingkat_kesulitan = $req->input('tingkatKesulitan') ?? 1;
            $insert->save();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }

    /**
    * @SWG\Patch(
    *      path="/master/kegiatan/{kegiatanId}",
    *      operationId="masterKegiatanUpdate",
    *      tags={"Update"},
    *      summary="Update Kegiatan",
    *      @SWG\Response(
    *          response=200,
    *          description="Berhasil Mengubah Data"
    *       ),
    *       @SWG\Response(response=400, description="Input Salah, atau Kurang"),
    *       @SWG\Response(response=401, description="Pengguna Belum Login/Mendapatkan Token"),
    *       @SWG\Parameter(in="path", required=true, name="kegiatanId", type="number", description="ID Kegiatan"),
    *       @SWG\Parameter(
    *           in="body",
    *           name="body",
    *           description="Update Kegiatan",
    *           required=true,
    *           @SWG\Schema(
    *               type="object",
    *               @SWG\Property(property="nama", type="string"),
    *               @SWG\Property(property="biaya", type="number"),
    *               @SWG\Property(property="waktu", type="number", description="Waktu dalam menit (default: 60menit)"),
    *               required={"nama", "biaya"}
    *           )
    *        ),
    *       security={"JWT": {}}
    *     )
    */
    public function update(Kegiatan $kegiatan, Request $req) {
        if(!$kegiatan) {
            return $this->errorMessage('Data tidak ditemukan.', 404);
        }

        $validator = Validator::make($req->all(), [
            'nama' => 'required|unique:m_kegiatan',
            'biaya' => 'required',
            'kode' => 'required|unique:m_kegiatan'
        ], $this->validatorMessage);

        if($validator->fails()) {
            return $this->errorMessage($this->validatorMessage($validator->errors()), 400);
        }

        try {
            $kegiatan->nama = $req->input('nama');
            $kegiatan->biaya = $req->input('biaya');
            $kegiatan->kode = $req->input('kode');
            $kegiatan->utama = $req->input('utama') ?? true;
            $kegiatan->tingkat_kesulitan = $req->input('tingkatKesulitan') ?? 1.0;
            $kegiatan->save();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }

    /**
    * @SWG\Delete(
    *      path="/master/kegiatan/{kegiatanId}",
    *      operationId="masterKegiatanHapus",
    *      tags={"Hapus"},
    *      summary="Hapus Kegiatan",
    *      @SWG\Response(
    *          response=200,
    *          description="Berhasil Menghapus Data"
    *       ),
    *       @SWG\Response(response=404, description="Data Tidak Ditemukan"),
    *       @SWG\Parameter(
    *           in="path",
    *           name="kegiatanId",
    *           description="ID Kegiatan",
    *           required=true,
    *           type="number"
    *       ),
    *       security={"JWT": {}}
    *     )
    */
    public function destroy(Kegiatan $kegiatan) {
        if(!$kegiatan) {
            return $this->errorMessage('Data tidak ditemukan.', 404);
        }

        try {
            $kegiatan->delete();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 400);
        }
    }
}
