<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Master\Review;
use Illuminate\Support\Facades\Validator;

class ReviewController extends Controller
{
    private $validatorMessage =  [
        'nama.required' => 'Nama dibutuhkan.',
        'nama.unique' => 'Nama agama yang sama telah ada di sistem.',
        'kategori.required' => 'Kategori review dibutuhkan.',
    ];


    /**
    * @SWG\Get(
    *      path="/master/review/{reviewId}",
    *      operationId="masterReviewId",
    *      tags={"Detail"},
    *      summary="Detail Review",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           in="path",
    *           name="reviewId",
    *           description="ID Review",
    *           required=true,
    *           type="number"
    *       ),
    *     )
*/
    public function show(Review $review) {
        if(!$review) {
            return $this->errorMessage('Data tidak ditemukan.', 404);
        }

        return $this->successMessageWithData($agama);
    }

    /**
    * @SWG\Get(
    *      path="/master/review",
    *      operationId="masterReviewDaftar",
    *      tags={"Daftar"},
    *      summary="Daftar Review",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Mengurutkan data berdasarkan nama",
    *           in="query",
    *           name="sort",
    *           enum={"desc", "asc"},
    *       ),
    *       @SWG\Parameter(
    *           type="number",
    *           description="Membatasi banyaknya data yang ditampilkan",
    *           in="query",
    *           name="limit"
    *       ),
    *       @SWG\Parameter(
    *           type="number",
    *           description="Halaman",
    *           in="query",
    *           name="page"
    *       ),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Pencarian data berdasarkan nama",
    *           in="query",
    *           name="search"
    *       ),
    *     )
    */
    public function index(Request $req) {
        $sort = $req->input('sort');
        $limit = $req->input('limit') ?? 10;
        $page = $req->input('page');
        $search = $req->input('search');

        $instance = new Review();

        $sortable = ['desc', 'asc'];

        $instance
            ->take($limit)
            ->when(in_array($sort, $sortable), function($q) use($sort) {
                $q->orderBy('nama', $sort);
            })
        ;

        $total = $instance->count();

        $instance
            ->when($page > 0, function($q) use ($page) {
                if(is_numeric($page)) {
                    $skip = ($page - 1) * $limit;
                    $q->skip($skip);
                }
            })
        ;

        $result = $instance->get();

        return $this->successMessageWithData($result, $total);
    }

    /**
    * @SWG\Post(
    *      path="/master/review",
    *      operationId="masterReviewTambah",
    *      tags={"Tambah"},
    *      summary="Tambah Review",
    *      @SWG\Response(
    *          response=200,
    *          description="Berhasil Menambah Data"
    *       ),
    *       @SWG\Response(response=400, description="Input Salah, atau Kurang"),
    *       @SWG\Response(response=401, description="Pengguna Belum Login/Mendapatkan Token"),
    *       @SWG\Parameter(
    *           in="body",
    *           name="body",
    *           description="Tambah Kegiatan",
    *           required=true,
    *           @SWG\Schema(
    *               type="object",
    *               @SWG\Property(property="nama", type="string"),
    *               @SWG\Property(property="kategori", type="number"),
    *               @SWG\Property(property="positif", type="boolean", description="Apakah review ini termasuk dalam nilai positif? (Default: true)"),
    *               required={"nama", "kategori"}
    *           )
    *        ),
    *       security={"JWT": {}}
    *     )
    */
    public function store(Request $req) {
        $validators = Validator::make($req->all(), [
            'nama' => 'required|unique:m_review',
            'kategori' => 'required'
        ], $this->validatorMessage);

        if($validators->fails()) {
            return $this->errorMessage($this->validatorMessage($validators->errors()), 400);
        }

        try {
            $insert = new Review();
            $insert->nama = $req->input('nama');
            $insert->kategori_review_id = $req->input('kategori');
            $insert->positif = $req->input('positif');
            $insert->save();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }

    /**
    * @SWG\Patch(
    *      path="/master/review/{reviewId}",
    *      operationId="masterReviewUpdate",
    *      tags={"Update"},
    *      summary="Update Review",
    *      @SWG\Response(
    *          response=200,
    *          description="Berhasil Mengubah Data"
    *       ),
    *       @SWG\Response(response=400, description="Input Salah, atau Kurang"),
    *       @SWG\Response(response=401, description="Pengguna Belum Login/Mendapatkan Token"),
    *       @SWG\Parameter(in="path", required=true, name="reviewId", description="ID Review", type="number"),
    *       @SWG\Parameter(
    *           in="body",
    *           name="body",
    *           description="Tambah Kegiatan",
    *           required=true,
    *           @SWG\Schema(
    *               type="object",
    *               @SWG\Property(property="nama", type="string"),
    *               @SWG\Property(property="kategori", type="number"),
    *               @SWG\Property(property="positif", type="boolean", description="Apakah review ini termasuk dalam nilai positif? (Default: true)"),
    *               required={"nama", "kategori"}
    *           )
    *        ),
    *       security={"JWT": {}}
    *     )
    */
    public function update(Review $review, Request $req) {
        if(!$review) {
            return $this->errorMessage('Data tidak ditemukan.', 404);
        }

        $validator = Validator::make($req->all(), [
            'nama' => 'required|unique:m_review',
            'kategori' => 'review'
        ], $this->validatorMessage);

        if($validator->fails()) {
            return $this->errorMessage($this->validatorMessage($validator->errors()), 400);
        }

        try {
            $review->nama = $req->input('nama');
            $review->kategori_review_id = $req->input('kategori');
            $review->positif = $req->input('positif');
            $review->save();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }

    /**
    * @SWG\Delete(
    *      path="/master/review/{reviewId}",
    *      operationId="masterReviewHapus",
    *      tags={"Hapus"},
    *      summary="Hapus Review",
    *      @SWG\Response(
    *          response=200,
    *          description="Berhasil Menghapus Data"
    *       ),
    *       @SWG\Response(response=404, description="Data Tidak Ditemukan"),
    *       @SWG\Parameter(
    *           in="path",
    *           name="reviewId",
    *           description="ID Review",
    *           required=true,
    *           type="number"
    *       ),
    *       security={"JWT": {}}
    *     )
    */
    public function destroy(Review $review) {
        if(!$review) {
            return $this->errorMessage('Data tidak ditemukan.', 404);
        }

        try {
            $review->delete();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 400);
        }
    }
}
