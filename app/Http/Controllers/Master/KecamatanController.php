<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Master\Kecamatan;
use Illuminate\Support\Facades\Validator;

class KecamatanController extends Controller
{
    private $validatorMessage =  [
        'nama.required' => 'Nama dibutuhkan.',
        'nama.unique' => 'Kecamatan yang sama telah ada di sistem.'
    ];


    /**
    * @SWG\Get(
    *      path="/master/kecamatan/{kecamatanId}",
    *      operationId="masterKecamatanId",
    *      tags={"Detail"},
    *      summary="Detail Kecamatan",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           in="path",
    *           name="kecamatanId",
    *           description="ID Kecamatan",
    *           required=true,
    *           type="number"
    *       ),
    *     )
    */
    public function show(Kecamatan $kecamatan) {
        if(!$kecamatan) {
            return $this->errorMessage('Data tidak ditemukan.', 404);
        }

        return $this->successMessageWithData($kecamatan);
    }

    /**
    * @SWG\Get(
    *      path="/master/kecamatan",
    *      operationId="masterKecamatanDaftar",
    *      tags={"Daftar"},
    *      summary="Daftar Kecamatan",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Mengurutkan data berdasarkan nama",
    *           in="query",
    *           name="sort",
    *           enum={"desc", "asc"},
    *       ),
    *       @SWG\Parameter(
    *           type="number",
    *           description="Membatasi banyaknya data yang ditampilkan",
    *           in="query",
    *           name="limit"
    *       ),
    *       @SWG\Parameter(
    *           type="number",
    *           description="Halaman",
    *           in="query",
    *           name="page"
    *       ),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Pencarian data berdasarkan nama",
    *           in="query",
    *           name="search"
    *       ),
    *     )
    */
    public function index(Request $req) {
        $sort = $req->input('sort');
        $limit = $req->input('limit') ?? 10;
        $page = $req->input('page');
        $search = $req->input('search');

        $instance = Kecamatan::take($limit)
            ->when(in_array($sort, $this->sortable), function($q) use($sort) {
                $q->orderBy('nama', $sort);
            })
            ->when(strlen($search) > 0, function($q) use($search) {
                $q->where('nama', 'like', "%{$search}%");
            })
        ;

        $total = $instance->count();

        $instance
            ->when($page > 0, function($q) use ($page) {
                if(is_numeric($page)) {
                    $skip = ($page - 1) * $limit;
                    $q->skip($skip);
                }
            })
        ;

        $result = $instance->get();

        return $this->successMessageWithData($result, $total);
    }

    /**
    * @SWG\Post(
    *      path="/master/kecamatan",
    *      operationId="masterKecamatanTambah",
    *      tags={"Tambah"},
    *      summary="Tambah Kecamatan",
    *      @SWG\Response(
    *          response=200,
    *          description="Berhasil Menambah Data"
    *       ),
    *       @SWG\Response(response=400, description="Input Salah, atau Kurang"),
    *       @SWG\Response(response=401, description="Pengguna Belum Login/Mendapatkan Token"),
    *       @SWG\Parameter(
    *           in="body",
    *           name="body",
    *           description="Tambah Kecamatan",
    *           required=true,
    *           @SWG\Schema(
    *               type="object",
    *               @SWG\Property(property="nama", type="string"),
    *               required={"nama"}
    *           )
    *        ),
    *       security={"JWT": {}}
    *     )
    */
    public function store(Request $req) {
        $validators = Validator::make($req->all(), [
            'nama' => 'required|unique:m_status_kepegawaian'
        ], $this->validatorMessage);

        if($validators->fails()) {
            return $this->errorMessage($this->validatorMessage($validators->errors()), 400);
        }

        $nama = $req->input('nama');

        try {
            $insert = new Kecamatan();
            $insert->nama = $nama;
            $insert->save();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }

    /**
    * @SWG\Patch(
    *      path="/master/kecamatan/{kecamatanId}",
    *      operationId="masterKecamatanUpdate",
    *      tags={"Update"},
    *      summary="Update Kecamatan",
    *      @SWG\Response(
    *          response=200,
    *          description="Berhasil Mengubah Data"
    *       ),
    *       @SWG\Response(response=400, description="Input Salah, atau Kurang"),
    *       @SWG\Response(response=401, description="Pengguna Belum Login/Mendapatkan Token"),
    *       @SWG\Parameter(in="path", name="kecamatanID", type="number", required=true),
    *       @SWG\Parameter(
    *           in="body",
    *           name="body",
    *           description="Ubah Kecamatan",
    *           required=true,
    *           @SWG\Schema(
    *               type="object",
    *               @SWG\Property(property="nama", type="string"),
    *               required={"nama"}
    *           )
    *        ),
    *       security={"JWT": {}}
    *     )
    */
    public function update(Kecamatan $kecamatan, Request $req) {
        if(!$kecamatan) {
            return $this->errorMessage('Data tidak ditemukan.', 404);
        }

        $validator = Validator::make($req->all(), [
            'nama' => 'required|unique:m_status_kepegawaian'
        ], $this->validatorMessage);

        if($validator->fails()) {
            return $this->errorMessage($this->validatorMessage($validator->errors()), 400);
        }

        try {
            $kecamatan->nama = $req->input('nama');
            $kecamatan->save();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }

    /**
    * @SWG\Delete(
    *      path="/master/kecamatan/{kecamatanId}",
    *      operationId="masterKecamatanId",
    *      tags={"Hapus"},
    *      summary="Hapus Kecamatan",
    *      @SWG\Response(
    *          response=200,
    *          description="Berhasil Menghapus Data"
    *       ),
    *       @SWG\Response(response=404, description="Data Tidak Ditemukan"),
    *       @SWG\Parameter(
    *           in="path",
    *           name="kecamatanId",
    *           description="ID Kecamatan",
    *           required=true,
    *           type="number"
    *       ),
    *       security={"JWT": {}}
    *     )
    */
    public function destroy(Kecamatan $kecamatan) {
        if(!$kecamatan) {
            return $this->errorMessage('Data tidak ditemukan.', 404);
        }

        try {
            $kecamatan->delete();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 400);
        }
    }
}
