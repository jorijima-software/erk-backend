<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Master\Golongan;
use Illuminate\Support\Facades\Validator;

class GolonganController extends Controller
{
    private $validatorMessage = [
        'nama.required' => 'Nama dibutuhkan.',
        'nama.unique' => 'Status yang sama telah terdaftar.',
        'kode.required' => 'Kode golongan dibutuhkan.',
        'kode.unique' => 'Kode golongan yang sama telah terdaftar.',
        'pangkat.required' => 'Nama pangkat golongan dibutuhkan.',
    ];


    /**
    * @SWG\Get(
    *      path="/master/golongan/{golonganId}",
    *      operationId="masterGolonganDetail",
    *      tags={"Detail"},
    *      summary="Detail Golongan",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=404, description="Data Tidak Ditemukan"),
    *       @SWG\Parameter(
    *           in="path",
    *           name="golonganId",
    *           description="ID Golongan",
    *           required=true,
    *           type="number"
    *       ),
    *     )
    */
    public function show(Golongan $golongan) {
        if (!$golongan) {
            return $this->errorMessage('Data tidak ditemukan.', 404);
        }

        return $this->successMessageWithData($golongan);
    }

    /**
    * @SWG\Get(
    *      path="/master/golongan",
    *      operationId="masterGolonganDaftar",
    *      tags={"Daftar"},
    *      summary="Daftar Golongan",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Data yang akan diurutkan",
    *           in="query",
    *           name="sort",
    *           enum={"kode", "nama", "pangkat"},
    *       ),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Mengurutkan data berdasarkan parameter sort",
    *           in="query",
    *           name="sortBy",
    *           enum={"desc", "asc"},
    *       ),
    *       @SWG\Parameter(
    *           type="number",
    *           description="Membatasi banyaknya data yang ditampilkan",
    *           in="query",
    *           name="limit"
    *       ),
    *       @SWG\Parameter(
    *           type="number",
    *           description="Halaman",
    *           in="query",
    *           name="page"
    *       ),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Pencarian data berdasarkan nama, kode, atau pangkat",
    *           in="query",
    *           name="search"
    *       ),
    *     )
    */
    public function index(Request $req) {
        $sort = $req->input('sort');
        $sortBy = $req->input('sortBy');
        $limit = $req->input('limit') ?? 10;
        $page = $req->input('page');
        $search = $req->input('search');

        $sorter = ['nama', 'kode', 'pangkat'];

        $instance = Golongan::take($limit)
            ->when(in_array($sort, $sorter) && in_array($sortBy, $this->sortable), function($q) use($sort, $sortBy) {
                $q->orderBy($sort, $sortBy);
            })
            ->when(strlen($search), function($query) use($search) {
                $query->where(function($q) use ($search) {
                    $q
                        ->where('nama', 'like', "%{$search}%")
                        ->where('kode', 'like', "%{$search}%")
                        ->where('pangkat', 'like', "%{$search}%");
                });
            })
        ;

        $total = $instance->count();

        $instance
            ->when($page > 0, function($q) use ($page) {
                if(is_numeric($page)) {
                    $skip = ($page - 1) * $limit;
                    $q->skip($skip);
                }
            })
        ;

        $result = $instance->get();

        return $this->successMessageWithData($result, $total);
    }

    /**
    * @SWG\Post(
    *      path="/master/golongan",
    *      operationId="masterGolonganTambah",
    *      tags={"Tambah"},
    *      summary="Tambah Golongan",
    *      @SWG\Response(
    *          response=200,
    *          description="Berhasil Menambah Data"
    *       ),
    *       @SWG\Response(response=400, description="Input Salah, atau Kurang"),
    *       @SWG\Parameter(
    *           in="body",
    *           name="body",
    *           description="Tambah Golongan",
    *           required=true,
    *           @SWG\Schema(
    *               type="object",
    *               @SWG\Property(property="nama", type="string"),
    *               @SWG\Property(property="kode", type="number"),
    *               @SWG\Property(property="pangkat", type="string")
    *           )
    *        ),
    *       security={"JWT": {}}
    *     )
    */
    public function store(Request $req) {
        $validators = Validator::make($req->all(), [
            'nama' => 'required|unique:m_golongan',
            'kode' => 'required|unique:m_golongan',
            'pangkat' => 'required'
        ], $this->validatorMessage);

        if($validators->fails()) {
            return $this->errorMessage($this->validatorMessage($validators->errors()), 400);
        }

        try {
            $insert = new Golongan();
            $insert->nama = $req->input('nama');
            $insert->kode = $req->input('kode');
            $insert->pangkat = $req->input('pangkat');
            $insert->save();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }

    /**
    * @SWG\Patch(
    *      path="/master/golongan/{golonganId}",
    *      operationId="masterGolonganTambah",
    *      tags={"Update"},
    *      summary="Update Golongan",
    *      @SWG\Response(
    *          response=200,
    *          description="Berhasil Mengubah Data"
    *       ),
    *       @SWG\Response(response=400, description="Input Salah, atau Data Kurang"),
    *       @SWG\Response(response=404, description="Data Tidak Ditemukan"),
    *       @SWG\Parameter(
    *           in="body",
    *           name="golonganId",
    *           description="ID Golongan",
    *           required=true,
    *           @SWG\Schema(
    *               type="object",
    *               @SWG\Property(property="nama", type="string"),
    *               @SWG\Property(property="kode", type="number"),
    *               @SWG\Property(property="golonganAwal", type="number"),
    *               @SWG\Property(property="golonganAkhir", type="number"),
    *               required={"nama", "kode", "pangkat"}
    *           )
    *        ),
    *       security={"JWT": {}}
    *     )
    */
    public function update(Golongan $golongan, Request $req) {
        if(!$status) {
            return $this->errorMessage('Data tidak ditemukan.', 404);
        }

        $validators = Validator::make($req->all(), [
            'nama' => 'required|unique:m_golongan',
            'kode' => 'required|unique:m_golongan',
            'pangkat' => 'required'
        ], $this->validatorMessage);

        if($validator->fails()) {
            return $this->errorMessage($this->validatorMessage($validator->errors()), 400);
        }

        try {
            $golongan->nama = $req->input('nama');
            $golongan->kode = $req->input('kode');
            $golongan->pangkat = $req->input('pangkat');
            $golongan->save();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }

    /**
    * @SWG\Delete(
    *      path="/master/golongan/{golonganId}",
    *      operationId="masterGolonganDelete",
    *      tags={"Hapus"},
    *      summary="Hapus Golongan",
    *      @SWG\Response(
    *          response=200,
    *          description="Berhasil Menghapus Data"
    *       ),
    *       @SWG\Response(response=404, description="Data Tidak Ditemukan"),
    *       @SWG\Parameter(
    *           in="path",
    *           name="golonganId",
    *           description="ID Golongan",
    *           required=true,
    *           type="number"
    *       ),
    *       security={"JWT": {}}
    *     )
    */
    public function destroy(Golongan $golongan) {
        if(!$status) {
            return $this->errorMessage('Data tidak ditemukan.', 404);
        }

        try {
            $golongan->delete();
            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 400);
        }
    }
}
