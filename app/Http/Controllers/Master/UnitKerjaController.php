<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Master\UnitKerja;
use Illuminate\Support\Facades\Validator;

class UnitKerjaController extends Controller
{
    protected $validatorMessages = [
        'nama.required' => 'Nama dibutuhkan',
        'satuanKerja.required' => 'Satuan kerja dibutuhkan.'
    ];

    /**
    * @SWG\Get(
    *      path="/master/unit-kerja/{unitKerjaId}",
    *      operationId="unitKerjaId",
    *      tags={"Detail"},
    *      summary="Detail Unit Kerja",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           in="path",
    *           name="unitKerjaId",
    *           description="ID Unit Kerja",
    *           required=true,
    *           type="number"
    *       ),
    *     )
    */
    public function show(UnitKerja $unitKerja) {
        if(!$unitKerja) {
            return $this->notFoundErrorMessage();
        }

        return $this->successMessageWithData($unitKerja);
    }

    /**
    * @SWG\Get(
    *      path="/master/unit-kerja",
    *      operationId="masterUnitKerjaDaftar",
    *      tags={"Daftar"},
    *      summary="Daftar Unit Kerja",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Mengurutkan data berdasarkan nama",
    *           in="query",
    *           name="sort",
    *           enum={"desc", "asc"},
    *       ),
    *       @SWG\Parameter(
    *           type="number",
    *           description="Membatasi banyaknya data yang ditampilkan",
    *           in="query",
    *           name="limit"
    *       ),
    *       @SWG\Parameter(
    *           type="number",
    *           description="Halaman",
    *           in="query",
    *           name="page"
    *       ),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Pencarian data berdasarkan nama",
    *           in="query",
    *           name="search"
    *       )
    *     )
    */
    public function index(Request $req) {
        $sort = $req->input('sort');
        $limit = $req->input('limit') ?? 10;
        $page = $req->input('page');
        $search = $req->input('search');

        $instance = UnitKerja::when(in_array($sort, $this->sortable), function($q) use ($sort) {
                $q->orderBy('nama', $sort);
            })
            ->take($limit)
            ->when(strlen($search), function($q) use ($search) {
                $q->where('nama', 'like', "%{$search}%");
            })
        ;

        $total = $instance->count();

        $instance->when($page > 0, function($q) use ($page, $limit) {
            $skip = ($page - 1) * $limit;
            $q->skip($skip);
        });

        $result = $instance->get();

        return $this->successMessageWithData($result);
    }

    /**
    * @SWG\Post(
    *      path="/master/unit-kerja",
    *      operationId="masterUnitKerjaTambah",
    *      tags={"Tambah"},
    *      summary="Tambah Unit Kerja",
    *      @SWG\Response(
    *          response=200,
    *          description="Berhasil Menambah Data"
    *       ),
    *       @SWG\Response(response=400, description="Input Salah, atau Kurang"),
    *       @SWG\Response(response=401, description="Pengguna Belum Login/Mendapatkan Token"),
    *       @SWG\Parameter(
    *           in="body",
    *           name="body",
    *           description="Tambah Unit Kerja",
    *           required=true,
    *           @SWG\Schema(
    *               type="object",
    *               @SWG\Property(property="nama", type="string"),
    *               @SWG\Property(property="satuanKerja", type="number", description="ID Satuan Kerja"),
    *               @SWG\Property(property="status", type="number"),
    *               required={"nama", "satuanKerja"}
    *           )
    *        ),
    *       security={"JWT": {}}
    *     )
    */
    public function store(Request $store) {
        $validator = Validator::make($req->all(), [
            'nama' => 'required',
            'satuanKerja' => 'required'
        ], $this->validatorMessages);

        if($validator->fails()) {
            return $this->errorMessage($this->validatorMessage($validator->errors()));
        }

        try {
            $insert = new UnitKerja();
            $insert->nama = $req->input('nama');
            $insert->satuan_kerja_id = $req->input('satuanKerja');
            $insert->status = $req->input('status') ?? 0;
            $insert->save();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }


    /**
    * @SWG\Delete(
    *      path="/master/unit-kerja/{unitKerjaId}",
    *      operationId="masterUnitKerjaHapus",
    *      tags={"Hapus"},
    *      summary="Hapus Unit Kerja",
    *      @SWG\Response(
    *          response=200,
    *          description="Berhasil Menghapus Data"
    *       ),
    *       @SWG\Response(response=404, description="Data Tidak Ditemukan"),
    *       @SWG\Parameter(
    *           in="path",
    *           name="tupoksiId",
    *           description="ID Unit Kerja",
    *           required=true,
    *           type="number"
    *       ),
    *       security={"JWT": {}}
    *     )
    */
    public function update(UnitKerja $unitKerja, Request $req) {
        if(!$unitKerja) {
            return $this->notFoundErrorMessage();
        }

        $validator = Validator::make($req->all(), [
            'nama' => 'required',
            'satuanKerja' => 'required'
        ], $this->validatorMessages);

        if($validator->fails()) {
            return $this->errorMessage($this->validatorMessage($validator->errors()));
        }

        try {
            $unitKerja->nama = $req->input('nama');
            $unitKerja->satuan_kerja_id = $req->input('satuanKerja');
            $unitKerja->status = $req->input('status') ?? 0;
            $unitKerja->save();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }
}
