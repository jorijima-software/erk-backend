<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Master\Agama;
use Illuminate\Support\Facades\Validator;

class AgamaController extends Controller
{
    private $validatorMessage =  [
        'nama.required' => 'Nama dibutuhkan.',
        'nama.unique' => 'Nama agama yang sama telah ada di sistem.'
    ];


    /**
    * @SWG\Get(
    *      path="/master/agama/{agamaId}",
    *      operationId="masterAgamaDetail",
    *      tags={"Detail"},
    *      summary="Detail Agama",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           in="path",
    *           name="agamaId",
    *           description="ID Agama",
    *           required=true,
    *           type="number"
    *       ),
    *     )
    */
    public function show(Agama $agama) {
        if(!$agama) {
            return $this->errorMessage('Data tidak ditemukan.', 404);
        }

        return $this->successMessageWithData($agama);
    }

    /**
    * @SWG\Get(
    *      path="/master/agama",
    *      operationId="masterAgamaDaftar",
    *      tags={"Daftar"},
    *      summary="Daftar Agama",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Mengurutkan data berdasarkan nama",
    *           in="query",
    *           name="sort",
    *           enum={"desc", "asc"},
    *       ),
    *       @SWG\Parameter(
    *           type="number",
    *           description="Membatasi banyaknya data yang ditampilkan",
    *           in="query",
    *           name="limit"
    *       ),
    *       @SWG\Parameter(
    *           type="number",
    *           description="Halaman",
    *           in="query",
    *           name="page"
    *       ),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Pencarian data berdasarkan nama",
    *           in="query",
    *           name="search"
    *       ),
    *     )
    */
    public function index(Request $req) {
        $sort = $req->input('sort');
        $limit = $req->input('limit') ?? 10;
        $page = $req->input('page');
        $search = $req->input('search');

        $instance = Agama::when(in_array($sort, $this->sortable), function($q) use($sort) {
                $q->orderBy('nama', $sort);
            })
            ->when(strlen($search) > 0, function($q) use($search) {
                $q->where('nama', 'like', "%{$search}%");
            })
        ;

        $total = $instance->count();

        $instance
            ->when($page > 0, function($q) use ($page, $limit) {
                if(is_numeric($page)) {
                    $skip = ($page - 1) * $limit;
                    $q->skip($skip);
                }
            })
            ->limit($limit)
        ;

        // dd($instance->toSql());

        $result = $instance->get();

        return $this->successMessageWithData($result, $total);
    }

    /**
    * @SWG\Post(
    *      path="/master/agama",
    *      operationId="masterAgamaTambah",
    *      tags={"Tambah"},
    *      summary="Tambah Agama",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           in="body",
    *           name="body",
    *           description="Tambah Agama",
    *           required=true,
    *           @SWG\Schema(
    *               type="object",
    *               @SWG\Property(property="nama", type="string")
    *           )
    *       ),
    *       security={{
    *           "JWT": {}
    *       }}
    *     )
    */
    public function store(Request $req) {
        $validators = Validator::make($req->all(), [
            'nama' => 'required|unique:m_agama'
        ], $this->validatorMessage);

        if($validators->fails()) {
            return $this->errorMessage($this->validatorMessage($validators->errors()), 400);
        }

        try {
            $insert = new Agama();
            $insert->nama = $req->input('nama');
            $insert->save();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }

    /**
    * @SWG\Patch(
    *      path="/master/agama/{agamaId}",
    *      operationId="masterAgamaUpdate",
    *      tags={"Update"},
    *      summary="Update Agama",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           in="body",
    *           name="agamaId",
    *           description="ID Agama",
    *           required=true,
    *           @SWG\Schema(
    *               type="object",
    *               @SWG\Property(property="nama", type="string"),
    *               required={"nama"}
    *           )
    *       ),
    *       security={"JWT": {}}
    *     )
    */
    public function update(Agama $agama, Request $req) {
        if(!$agama) {
            return $this->errorMessage('Data tidak ditemukan.', 404);
        }

        $validator = Validator::make($req->all(), [
            'nama' => 'required|unique:m_agama'
        ], $this->validatorMessage);

        if($validator->fails()) {
            return $this->errorMessage($this->validatorMessage($validator->errors()), 400);
        }

        try {
            $agama->nama = $req->input('nama');
            $agama->save();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }

    /**
    * @SWG\Delete(
    *      path="/master/agama/{agamaId}",
    *      operationId="masterAgamaDelete",
    *      tags={"Hapus"},
    *      summary="Hapus Agama",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           in="path",
    *           name="agamaId",
    *           description="ID Agama",
    *           required=true,
    *           type="number"
    *       ),
    *       security={{
    *           "JWT": {}
    *       }}
    *     )
    */
    public function destroy(Agama $agama) {
        if(!$agama) {
            return $this->errorMessage('Data tidak ditemukan.', 404);
        }

        try {
            $agama->delete();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 400);
        }
    }
}
