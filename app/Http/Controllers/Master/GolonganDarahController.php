<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Master\GolonganDarah;
use Illuminate\Support\Facades\Validator;

class GolonganDarahController extends Controller
{
    private $validatorMessage =  [
        'nama.required' => 'Nama dibutuhkan.',
        'nama.unique' => 'Golongan darah yang sama telah ada di sistem.'
    ];


    /**
    * @SWG\Get(
    *      path="/master/golongan-darah/{golonganDarahId}",
    *      operationId="masterGolonganDarahDetail",
    *      tags={"Detail"},
    *      summary="Detail Golongan Darah",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           in="path",
    *           name="golonganDarahId",
    *           description="ID Golongan Darah",
    *           required=true,
    *           type="number"
    *       ),
    *     )
    */
    public function show(GolonganDarah $golonganDarah) {
        if(!$golonganDarah) {
            return $this->errorMessage('Data tidak ditemukan.', 404);
        }

        return $this->successMessageWithData($golonganDarah);
    }

    /**
    * @SWG\Get(
    *      path="/master/golongan-darah",
    *      operationId="masterGolonganDarahDaftar",
    *      tags={"Daftar"},
    *      summary="Daftar Golongan Darah",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Mengurutkan data berdasarkan nama",
    *           in="query",
    *           name="sort",
    *           enum={"desc", "asc"},
    *       ),
    *       @SWG\Parameter(
    *           type="number",
    *           description="Membatasi banyaknya data yang ditampilkan",
    *           in="query",
    *           name="limit"
    *       ),
    *       @SWG\Parameter(
    *           type="number",
    *           description="Halaman",
    *           in="query",
    *           name="page"
    *       ),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Pencarian data berdasarkan nama",
    *           in="query",
    *           name="search"
    *       ),
    *     )
    */
    public function index(Request $req) {
        $sort = $req->input('sort');
        $limit = $req->input('limit') ?? 10;
        $page = $req->input('page');
        $search = $req->input('search');

        $instance = GolonganDarah::take($limit)
            ->when(in_array($sort, $this->sortable), function($q) use($sort) {
                $q->orderBy('nama', $sort);
            })
            ->when(strlen($search), function($q) use ($search) {
                $q->where('nama', 'like', "{$search}");
            })
        ;

        $total = $instance->count();

        $instance
            ->when($page > 0, function($q) use ($page) {
                if(is_numeric($page)) {
                    $skip = ($page - 1) * $limit;
                    $q->skip($skip);
                }
            })
        ;

        $result = $instance->get();

        return $this->successMessageWithData($result, $total);
    }

    /**
    * @SWG\Post(
    *      path="/master/golongan-darah",
    *      operationId="masterGolonganDarahTambah",
    *      tags={"Tambah"},
    *      summary="Tambah Golongan Darah",
    *      @SWG\Response(
    *          response=200,
    *          description="Berhasil Menambah Data"
    *       ),
    *       @SWG\Response(response=400, description="Input Salah, atau Kurang"),
    *       @SWG\Parameter(
    *           in="body",
    *           name="body",
    *           description="Tambah Golongan Darah",
    *           required=true,
    *           @SWG\Schema(
    *               type="object",
    *               @SWG\Property(property="nama", type="string"),
    *               required={"nama"}
    *           )
    *        ),
    *       security={"JWT": {}}
    *     )
    */
    public function store(Request $req) {
        $validators = Validator::make($req->all(), [
            'nama' => 'required|unique:m_golongan_darah'
        ], $this->validatorMessage);

        if($validators->fails()) {
            return $this->errorMessage($this->validatorMessage($validators->errors()), 400);
        }

        try {
            $insert = new GolonganDarah();
            $insert->nama = $req->input('nama');
            $insert->save();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }

    public function update(GolonganDarah $golonganDarah, Request $req) {
        if(!$golonganDarah) {
            return $this->errorMessage('Data tidak ditemukan.', 404);
        }

        $validator = Validator::make($req->all(), [
            'nama' => 'required|unique:m_golongan_darah'
        ], $this->validatorMessage);

        if($validator->fails()) {
            return $this->errorMessage($this->validatorMessage($validator->errors()), 400);
        }

        try {
            $golonganDarah->nama = $req->input('nama');
            $golonganDarah->save();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }

    /**
    * @SWG\Delete(
    *      path="/master/golongan-darah/{golonganDarahId}",
    *      operationId="masterGolonganDelete",
    *      tags={"Hapus"},
    *      summary="Hapus Golongan Darah",
    *      @SWG\Response(
    *          response=200,
    *          description="Berhasil Menghapus Data"
    *       ),
    *       @SWG\Response(response=404, description="Data Tidak Ditemukan"),
    *       @SWG\Parameter(
    *           in="path",
    *           name="golonganDarahId",
    *           description="ID Golongan Darah",
    *           required=true,
    *           type="number"
    *       ),
    *       security={"JWT": {}}
    *     )
    */
    public function destroy(GolonganDarah $golonganDarah) {
        return $golonganDarah;
        if(!$golonganDarah) {
            return $this->errorMessage('Data tidak ditemukan.', 404);
        }

        try {
            $golonganDarah->delete();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 400);
        }
    }
}
