<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Master\SatuanKerja;
use Illuminate\Support\Facades\Validator;

class SatuanKerjaController extends Controller
{
    private $validatorMessage = [];


    /**
    * @SWG\Get(
    *      path="/master/satuan-kerja/{satuanKerjaId}",
    *      operationId="satuanKerjaDetail",
    *      tags={"Detail"},
    *      summary="Detail Satuan Kerja",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           in="path",
    *           name="satuanKerjaId",
    *           description="ID Satuan Kerja",
    *           required=true,
    *           type="number"
    *       ),
    *     )
    */
    public function show(SatuanKerja $satuanKerja) {
        if (!$satuanKerja) {
            return $this->errorMessage('Data tidak ditemukan.', 404);
        }

        return $this->successMessageWithData($satuanKerja);
    }

    /**
    * @SWG\Get(
    *      path="/master/satuan-kerja",
    *      operationId="masterSatuanKerja",
    *      tags={"Daftar"},
    *      summary="Daftar Satuan Kerja",
    *      @SWG\Response(
    *          response=200,
    *          description="Success"
    *       ),
    *       @SWG\Response(response=400, description="Bad Request"),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Mengurutkan data berdasarkan nama",
    *           in="query",
    *           name="sort",
    *           enum={"desc", "asc"},
    *       ),
    *       @SWG\Parameter(
    *           type="number",
    *           description="Membatasi banyaknya data yang ditampilkan",
    *           in="query",
    *           name="limit"
    *       ),
    *       @SWG\Parameter(
    *           type="number",
    *           description="Halaman",
    *           in="query",
    *           name="page"
    *       ),
    *       @SWG\Parameter(
    *           type="string",
    *           description="Pencarian data berdasarkan nama",
    *           in="query",
    *           name="search"
    *       )
    *     )
    */
    public function index(Request $req) {
        $sort = $req->input('sort');
        $limit = $req->input('limit') ?? 10;
        $page = $req->input('page');
        $search = $req->input('search');

        $instance = SatuanKerja::take($limit)
            ->when(in_array($sort, $sortable), function($q) use($sort) {
                $q->orderBy('nama', $sort);
            })
            ->when(strlen($search) > 0, function($q) use($search) {
                $q->where('nama', 'like', "%{$search}%");
            })
        ;

        $total = $instance->count();

        $instance
            ->when($page > 0, function($q) use ($page) {
                if(is_numeric($page)) {
                    $skip = ($page - 1) * $limit;
                    $q->skip($skip);
                }
            })
        ;

        $result = $instance->get();

        return $this->successMessageWithData($result, $total);
    }

    /**
    * @SWG\Post(
    *      path="/master/satuan-kerja",
    *      operationId="masterSatuanKerjaTambah",
    *      tags={"Tambah"},
    *      summary="Tambah Satuan Kerja",
    *      @SWG\Response(
    *          response=200,
    *          description="Berhasil Menambah Data"
    *       ),
    *       @SWG\Response(response=400, description="Input Salah, atau Kurang"),
    *       @SWG\Response(response=401, description="Pengguna Belum Login/Mendapatkan Token"),
    *       @SWG\Parameter(
    *           in="body",
    *           name="body",
    *           description="Tambah Satuan Kerja",
    *           required=true,
    *           @SWG\Schema(
    *               type="object",
    *               @SWG\Property(property="nama", type="string"),
    *               @SWG\Property(property="kode", type="number", description="Kode Satuan Kerja"),
    *               @SWG\Property(property="eselon", type="number", description="Kode Eselon (!!BUKAN ID!!)"),
    *               required={"nama", "kode", "eselon"}
    *           )
    *        ),
    *       security={"JWT": {}}
    *     )
    */
    public function store(Request $req) {
        $validators = Validator::make($req->all(), [
            'nama' => 'required|unique:m_satuan_kerja',
            'kode' => 'required',
            'eselon' => 'required',
        ], $this->validatorMessage);

        if($validators->fails()) {
            return $this->errorMessage($this->validatorMessage($validators->errors()), 400);
        }

        try {
            $insert = new SatuanKerja();
            $insert->nama = $req->input('nama');
            $insert->kode = $req->input('kode');
            $insert->eselon = $req->input('eselon');
            $insert->save();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }

    /**
    * @SWG\Patch(
    *      path="/master/satuan-kerja/{satuanKerjaId}",
    *      operationId="masterSatuanKerjaUpdate",
    *      tags={"Update"},
    *      summary="Update Satuan Kerja",
    *      @SWG\Response(
    *          response=200,
    *          description="Berhasil Mengubah Data"
    *       ),
    *       @SWG\Response(response=400, description="Input Salah, atau Kurang"),
    *       @SWG\Response(response=401, description="Pengguna Belum Login/Mendapatkan Token"),
    *       @SWG\Parameter(in="path", type="number", required=true, name="satuanKerjaId", description="ID Satuan Kerja"),
    *       @SWG\Parameter(
    *           in="body",
    *           name="body",
    *           description="Tambah Satuan Kerja",
    *           required=true,
    *           @SWG\Schema(
    *               type="object",
    *               @SWG\Property(property="nama", type="string"),
    *               @SWG\Property(property="kode", type="number", description="Kode Satuan Kerja"),
    *               @SWG\Property(property="eselon", type="number", description="Kode Eselon (!!BUKAN ID!!)"),
    *               required={"nama", "kode", "eselon"}
    *           )
    *        ),
    *       security={"JWT": {}}
    *     )
    */
    public function update(SatuanKerja $satuanKerja, Request $req) {
        if(!$satuanKerja) {
            return $this->errorMessage('Data tidak ditemukan.', 404);
        }

        $validators = Validator::make($req->all(), [
            'nama' => 'required|unique:m_satuan_kerja',
            'kode' => 'required',
            'eselon' => 'required',
        ], $this->validatorMessage);

        if($validators->fails()) {
            return $this->errorMessage($this->validatorMessage($validators->errors()), 400);
        }

        try {
            $satuanKerja->nama = $req->input('nama');
            $satuanKerja->kode = $req->input('kode');
            $satuanKerja->eselon = $req->input('eselon');
            $satuanKerja->save();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }

    /**
    * @SWG\Delete(
    *      path="/master/satuan-kerja/{satuanKerjaId}",
    *      operationId="masterSatuanKerjaHapus",
    *      tags={"Hapus"},
    *      summary="Hapus Satuan Kerja",
    *      @SWG\Response(
    *          response=200,
    *          description="Berhasil Menghapus Data"
    *       ),
    *       @SWG\Response(response=404, description="Data Tidak Ditemukan"),
    *       @SWG\Parameter(
    *           in="path",
    *           name="satuanKerjaId",
    *           description="ID Satuan Kerja",
    *           required=true,
    *           type="number"
    *       ),
    *       security={"JWT": {}}
    *     )
    */
    public function destroy(SatuanKerja $satuanKerja) {
        if(!$satuanKerja) {
            return $this->errorMessage('Data tidak ditemukan.', 404);
        }

        try {
            $satuanKerja->delete();

            return $this->successMessage();
        } catch (\Exception $e) {
            return $this->errorMessage($e->getMessage(), 500);
        }
    }
}
