<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * @SWG\Swagger(
 *     basePath="/api",
 *     schemes={"http"},
 *     host="erk-backend.test",
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="@ragasubekti/ERK",
 *         description="Dokumentasi API ERK",
 *         @SWG\Contact(
 *             email="ragasubekti@outlook.com"
 *         ),
 *     )
 * )
 */

 /**
 * @SWG\SecurityScheme(
 *      securityDefinition="JWT",
 *      type="apiKey",
 *      in="header",
 *      name="Authorization",
 *      scopes={},
 *      description="Untuk mengakses beberapa API dibutuhkan JWT, contoh header: Bearer xxxx.xxxxxxxxxx.xxxx"
 * )
 */


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $sortable = ['desc', 'asc'];
    protected $validatorMessages = [
        'nama.required' => 'Nama dibutuhkan.'
    ];

    public function __construct() {
        $this->middleware([\App\Http\Middleware\VerifyJWTToken::class]);
    }

    public function validatorMessage($errors) {
        $errors = collect($errors);
        $messages = [];

        foreach($errors->all() as $message) {
            array_push($messages, $message[0]);
        }

        if(count($messages) > 1) {
            return $messages;
        }

        return $messages[0];
    }

    public function errorMessage($message, $code = 400) {
        return response()->json([
            'error' => true,
            'message' => $message,
            'status' => ['code' => $code, 'error' => true],
            'executionTime' => microtime(true) - LARAVEL_START,
        ], $code);
    }

    public function notFoundErrorMessage() {
        return $this->errorMessage('Data tidak ditemukan.', 404);
    }

    public function successMessage($code = 200) {
        return response()->json([
            'success' => true,
            'status' => ['code' => $code, 'error' => false],
            'executionTime' => microtime(true) - LARAVEL_START,
        ], $code);
    }

    public function successMessageWithData($data, $total = false, $code = 200) {
        $response = [
            'success' => true,
            'data' => $data,
            'status' => ['code' => $code, 'error' => false],
            'executionTime' => microtime(true) - LARAVEL_START,
        ];

        if(!is_bool($total)) {
            $response['total'] = $total;
        }

        return response()->json($response, $code);
    }
}
