<?php

namespace App\Http\Controllers\Aktifitas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\InputAktifitas;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;

class InputAktifitasController extends Controller
{
    public function __construct() {
        $this->middleware('\App\Http\Middleware\VerifyJWTToken');
    }

    public function index(Request $req) {
        $user = JWTAuth::parseToken()->toUser();

        if(!$user) {
            return $this->errorMessage('Anda tidak memiliki akses ke halaman ini.', 401);
        }

        $limit = $req->input('limit') ?? 10;
        $page = $req->input('page');
        $search = $req->input('search');
        $nip = $req->input('nip');

        $validator = Validator::make($req->all(), [
            'nip.required' => $user->level <= 2 ? 'required' : ''
        ]);

        if($validator->fails()) {
            return $this->errorMessage('Pegawai belum dipilih.', 400);
        }

        $instance = InputAktifitas::take($limit)
            ->when($user->level > 2, function($q) use ($user) {
                $q->where('nip', $user->nip);
            })
            ->when($user->level <= 2, function($q) use ($nip) {
                $q->where('nip', $nip);
            })
            ->when(strlen($search) > 0, function($q) use ($search) {
                $q->where('nama', 'like', "%{$search}%");
            })
            // ->when()
        ;

        $total = $instance->count();

        $instance->when($page > 1, function($q) use ($page, $limit) {
            $skip = ($page - 1) * $limit;
            $q->skip($skip);
        });

        $result = $instance->get();

        return $this->successMessageWithData($result, $total);
    }

    public function store(Request $req) {

    }
}
