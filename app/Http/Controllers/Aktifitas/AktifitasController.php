<?php

namespace App\Http\Controllers\Aktifitas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Validation\Validator;
use App\Model\Skp;
use App\Http\Resources\Aktifitas\AktifitasResource;

class AktifitasController extends Controller
{
    public function __construct() {
        $this->middleware('\App\Http\Middleware\VerifyJWTToken');
    }

    public function indexAktifitasUtama(Request $req) {
        $user = JWTAuth::parseToken()->toUser();

        $sortBy = $req->input('sortBy');
        $limit = $req->input('limit') ?? 10;
        $page = $req->input('page');

        $instance = Skp::take($limit)
            ->where('nip', $user->nip)
            // ->when(in_array($sortBy, $this->sortable), function($q) use ($sortBy) {
            //     $q->where('');
            // })`
            ->whereHas('kegiatan', function($q) {
                $q->where('utama', true);
            })
            ->orWhereHas('tupoksi', function($q) {
                $q->where('utama', true);
            })
        ;

        $total = $instance->count();
        $result = AktifitasResource::collection($instance->get());

        return $this->successMessageWithData($result, $total);
    }
}
