<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class InputReview extends Model
{
    protected $table = 'input_review';
    protected $guarded = [];
    public $timestamps = true;

    public function pegawai() {
        return $this->hasOne('App\Model\Pegawai', 'nip', 'nip');
    }

    public function reviewer() {
        return $this->hasOne('App\Model\Pegawai', 'nip', 'nip_reviewer');
    }

    public function review() {
        return $this->hasOne('App\Model\Master\Review', 'id', 'review_id');
    }
}
