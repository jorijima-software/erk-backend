<?php

namespace App\Model\Auth;

use Illuminate\Database\Eloquent\Model;

use App\Model\Pegawai;
class User extends Model
{
    protected $table = 'master_user';

    public function pegawai() {
        $this->hasOne(Pegawai, 'nip', 'nip');
    }
}
