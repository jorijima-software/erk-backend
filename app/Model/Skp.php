<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Skp extends Model
{
    protected $table = 'skp';
    protected $guarded = [];
    public $timestamps = true;

    public function kegiatan() {
        return $this->hasOne('App\Model\Master\Kegiatan', 'id', 'kegiatan_id');
    }

    public function tupoksi() {
        return $this->hasOne('App\Model\Master\Tupoksi', 'id', 'tupoksi_id');
    }

    public function tipeOutput() {
        return $this->hasOne('App\Model\Master\SkpOutput', 'id', 'skp_output_id');
    }

    public function tipeWaktu() {
        return $this->hasOne('App\Model\Master\TipeWaktu', 'id', 'tipe_waktu_id');
    }
}
