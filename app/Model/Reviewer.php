<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
// use Tymon\JWTAuth\Contracts\Validator;

class Reviewer extends Model
{
    protected $table = 'reviewer';

    public function scopeReviewer($q, $user, $kode = false, $search = false) {
        return $q->when($user->level == 2, function($mq) use ($user) {
            $mq->whereHas('pegawai', function($query) use($user) {
                $query->whereHas('satuanKerja', function($q) use ($user) {
                    $q->where('kode', $user->nip);
                });
            });
        })
        ->when($user->level < 2, function($mq) use ($kode) {
            $mq->whereHas('pegawai', function($query) use($kode) {
                $query->whereHas('satuanKerja', function($q) use ($kode) {
                    $q->where('kode', $kode);
                });
            });
        })
        ->when(strlen($search) > 0, function($q) use ($search) {
            $q->where('nama', 'like', "%{$search}%");
        });
    }

    public static function noReviewer($user, $kode = false) {
        return Pegawai::when($user->level == 2, function($query) use ($user) {
            $query->whereHas('satuanKerja', function($q) use ($user) {
                $q->where('kode', $user->nip);
            });
        })
        ->when($user->level < 2, function($query) use ($kode) {
            $query->whereHas('satuanKerja', function($q) use ($kode) {
                $q->where('kode', $kode);
            });
        })
        ->whereDoesntHave('mereview');
    }

    public static function withoutReviewer($user, $kode = false) {
        return Pegawai::when($user->level < 2, function($q) use ($kode) {
            $q->whereHas('satuanKerja', function($qw) use ($kode) {
               $qw->where('kode', $kode);
            });
        })
        ->when($user->level == 2, function($q) use ($user) {
            $q->whereHas('satuanKerja', function($qw) use ($user) {
                $qw->where('kode', $user->nip);
            });
        })
        ->whereDoesntHave('reviewer');
    }

    public function pegawai() {
        return $this->hasOne('App\Model\Pegawai', 'nip', 'nip');
    }

    public function reviewer() {
        return $this->hasOne('App\Model\Pegawai', 'nip', 'nip_reviewer');
    }
}
