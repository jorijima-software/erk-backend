<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class Golongan extends Model
{
    protected $table = 'm_golongan';
    protected $guarded = [];
    public $timestamps = false;
}
