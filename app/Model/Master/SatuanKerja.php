<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;
use App\Model\Master\Kecamatan;

class SatuanKerja extends Model
{
    protected $table = 'm_satuan_kerja';
    protected $guarded = [];
    public $timestamps = false;

    public function kecamatan() {
        return $this->hasOne(Kecamatan, 'kecamatan_id', 'id');
    }

    public function eselon() {
        return $this->hasOne(Eselon, 'kode', 'eselon_kode');
    }
}