<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class Tupoksi extends Model
{
    protected $table = 'm_tupoksi';
    protected $guarded = [];
    public $timestamps = false;

    public function jabatan() {
        return $this->belongsTo('App\Model\Master\Jabatan', 'jabatan_id', 'id');
    }
}
