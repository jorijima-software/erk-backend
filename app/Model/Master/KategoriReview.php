<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class KategoriReview extends Model
{
    protected $table = 'm_kategori_review';
    protected $guarded = [];
    public $timestamps = false;
}
