<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class Jabatan extends Model
{
    protected $table = 'm_jabatan';
    protected $guarded = [];
    public $timestamps = false;

    public function eselon() {
        return $this->hasOne('App\Model\Master\Eselon', 'id', 'eselon_id');
    }

    public function atasan() {
        return $this->hasOne('App\Model\Master\Jabatan', 'kode',
        'kode_parent');
    }

    public function bawahan() {
        return $this->hasOne('App\Model\Master\Jabatan', 'kode_parent', 'kode');
    }

    public function jenisJabatan() {
        return $this->hasOne('App\Model\Master\JenisJabatan', 'id', 'jenis_id');
    }

    public function pegawai() {
        return $this->hasMany('App\Model\Pegawai', 'jabatan_id', 'id');
    }
}
