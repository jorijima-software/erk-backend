<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class SkpOutput extends Model
{
    protected $table = 'm_skp_output';
    protected $fillable = ['nama'];
    public $timestamps = false;
}
