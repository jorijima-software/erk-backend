<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class UnitKerja extends Model
{
    protected $table = 'm_unit_kerja';

    public function unitKerja() {
        return $this->hasMany(SatuanKerja, 'id', 'satuan_kerja_id');
    }
}
