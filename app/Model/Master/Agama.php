<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class Agama extends Model
{
    protected $table = 'm_agama';
    protected $fillable = ['nama'];
    public $timestamps = false;
}
