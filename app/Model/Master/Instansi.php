<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class Instansi extends Model
{
    protected $table = 'm_instansi';
    public $timestamps = false;
}
