<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class Eselon extends Model
{
    protected $table = 'm_eselon';
    protected $guarded = [];
    public $timestamps = false;

    public function golongan($arr) {
        return Golongan::whereBetween('id', $arr)->get();
    }
}
