<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Aktifitas extends Model
{
    use SoftDeletes;

    protected $table = 'm_aktifitas';
    protected $dates = ['deleted_at'];
}
