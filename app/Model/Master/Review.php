<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table = 'm_review';
    protected $guarded = [];
    public $timestamps = false;

    public function kategori() {
        return $this->hasOne(\App\Model\Master\KategoriReview, 'id', 'kategori_review_id');
    }
}
