<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class TipeWaktu extends Model
{
    protected $table = 'm_tipe_waktu';
}
