<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class InputAktifitas extends Model
{
    protected $table = 'input_aktifitas';

    public function skp() {
        return $this->hasOne('App\Model\Skp', 'id', 'skp_id');
    }

}
