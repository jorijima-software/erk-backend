<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/authentication', 'User\UserController@login');
Route::post('/change-password', 'User\UserController@changePassword');
/**
 * Master Data API
 */
Route::apiResource('/skp', 'Skp\SkpController');

Route::prefix('master')->group(function() {
    Route::apiResource('golongan', 'Master\GolonganController');
    Route::apiResource('agama', 'Master\AgamaController');
    Route::apiResource('kecamatan', 'Master\KecamatanController');
    Route::apiResource('kegiatan', 'Master\KegiatanController');
    Route::apiResource('review', 'Master\ReviewController');
    Route::apiResource('eselon', 'Master\EselonController');
    Route::apiResource('jabatan', 'Master\JabatanController');
    Route::apiResource('tupoksi', 'Master\TupoksiController');
    Route::apiResource('unit-kerja', 'Master\UnitKerjaController');
    Route::apiResource('jenis-jabatan', 'Master\JenisJabatanController');
    Route::apiResource('status-kepegawaian', 'Master\StatusKepegawaianController');
    Route::apiResource('golongan-darah', 'Master\GolonganDarahController');
    Route::apiResource('kategori-review', 'Master\KategoriReviewController');
    Route::apiResource('satuan-kerja', 'Master\SatuanKerjaController');
    Route::apiResource('skp-output', 'Master\SkpOutputController'); 
    Route::get('tipe-waktu', 'Master\TipeWaktuController@index');
});

Route::prefix('pegawai')->group(function() {
    Route::namespace('Pegawai')->group(function() {
        Route::get('profile', 'PegawaiController@profile');
        Route::get('struktur', 'PegawaiController@strukturPegawai');
        Route::get('struktur/{id}', 'PegawaiController@strukturPegawai');
        Route::get('reviewer', 'ReviewerController@index');
        Route::get('belum-mereview', 'ReviewerController@belumMereview');
    });

    Route::get('aktifitas', 'Aktifitas\InputAktifitasController@index');
});

Route::namespace('Aktifitas')->prefix('aktifitas')->group(function() {
    Route::get('utama', 'AktifitasController@indexAktifitasUtama');
});

Route::prefix('reviewer')->group(function() {
    Route::get('{nip}/atasan', 'Pegawai\ReviewerController@daftarReviewerAtasan');
    Route::get('{nip}/bawahan', 'Pegawai\ReviewerController@daftarReviewerBawahan');
    Route::get('{nip}/rekan', 'Pegawai\ReviewerController@daftarReviewerRekanKerja');
    Route::post('{nip}', 'Pegawai\ReviewerController@setReviewer');
});