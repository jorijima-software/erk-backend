<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MasterTipeWaktuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['id' => 1, 'nama' => 'Hari', 'multiplier' => 1],
            ['id' => 2, 'nama' => 'Minggu', 'multiplier' => 7],
            ['id' => 3, 'nama' => 'Bulan', 'multiplier' => 30],
            ['id' => 4, 'nama' => 'Tahun', 'multiplier' => 365],
        ];

        foreach($data as $insert) {
            DB::table('m_tipe_waktu')->insert($insert);
        }
    }
}
