<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MasterJenisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'nama' => 'Jabatan Struktural'
            ],
            [
                'id' => 2,
                'nama' => 'Jabatan Fungsional Tertentu'
            ],
            [
                'id' => 3,
                'nama' => 'Jabatan Fungsional Umum'
            ]
        ];

        foreach($data as $insert) {
            DB::table('m_jenis_jabatan')->insert($insert);
        }
    }
}
