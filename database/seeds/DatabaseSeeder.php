<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            MasterJenisSeeder::class,
            MasterGolonganSeeder::class,
            MasterAgamaSeeder::class,
            MasterEselonSeeder::class,
            MasterUserSeeder::class,
            MasterTipeWaktuSeeder::class,
        ]);
    }
}
