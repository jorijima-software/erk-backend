<?php

use Illuminate\Database\Seeder;

class MasterUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'nip' => 'root',
            'password' => '$2y$10$u9hVCm3e3N23/vpnbA4.V.l5XO6Yc47XOJC84SMYi9Wy0fI8uZfdC',
            'level' => '0',
            'enable' => true,
        ];

        DB::table('master_user')->insert($data);
    }
}
