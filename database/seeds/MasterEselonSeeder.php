<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MasterEselonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['id' => 1, 'nama' => 'I.a', 'kode' => 11, 'golongan_awal_id' => 16, 'golongan_akhir_id' => 17],
            ['id' => 2, 'nama' => 'I.b', 'kode' => 12, 'golongan_awal_id' => 15, 'golongan_akhir_id' => 17],
            ['id' => 3, 'nama' => 'II.a', 'kode' => 21, 'golongan_awal_id' => 15, 'golongan_akhir_id' => 16],
            ['id' => 4, 'nama' => 'II.b', 'kode' => 22, 'golongan_awal_id' => 14, 'golongan_akhir_id' => 15],
            ['id' => 5, 'nama' => 'III.a', 'kode' => 31, 'golongan_awal_id' => 13, 'golongan_akhir_id' => 14],
            ['id' => 6, 'nama' => 'III.b', 'kode' => 32, 'golongan_awal_id' => 12, 'golongan_akhir_id' => 13],
            ['id' => 7, 'nama' => 'IV.a', 'kode' => 41, 'golongan_awal_id' => 11, 'golongan_akhir_id' => 12],
            ['id' => 8, 'nama' => 'IV.b', 'kode' => 42, 'golongan_awal_id' => 10, 'golongan_akhir_id' => 11],
            ['id' => 9, 'nama' => 'IV.c', 'kode' => 43, 'golongan_awal_id' => 9, 'golongan_akhir_id' => 10],
        ];

        foreach ($data as $insert) {
            DB::table('m_eselon')->insert($insert);
        }


    }
}
