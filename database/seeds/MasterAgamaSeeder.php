<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MasterAgamaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['id' => 1, 'nama' => 'Islam'],
            ['id' => 2, 'nama' => 'Kristen'],
            ['id' => 3, 'nama' => 'Protestan'],
            ['id' => 4, 'nama' => 'Hindu'],
            ['id' => 5, 'nama' => 'Buddha'],
            ['id' => 6, 'nama' => 'Kong Hu Cu'],
        ];

        foreach($data as $insert) {
            DB::table('m_agama')->insert($insert);
        }
    }
}
