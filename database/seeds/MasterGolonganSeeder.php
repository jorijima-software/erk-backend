<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MasterGolonganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'nama' => 'I/a',
                'kode' => 11,
                'pangkat' => 'Juru Muda'
            ],
            [
                'id' => 2,
                'nama' => 'I/b',
                'kode' => 12,
                'pangkat' => 'Juru Muda Tingkat I'
            ],
            [
                'id' => 3,
                'nama' => 'I/c',
                'kode' => 13,
                'pangkat' => 'Juru'
            ],
            [
                'id' => 4,
                'nama' => 'I/d',
                'kode' => 14,
                'pangkat' => 'Juru Tingkat I'
            ],
            [
                'id' => 5,
                'nama' => 'II/a',
                'kode' => 21,
                'pangkat' => 'Pengatur Muda'
            ],
            [
                'id' => 6,
                'nama' => 'II/b',
                'kode' => 22,
                'pangkat' => 'Pengatur Muda Tingkat I'
            ],
            [
                'id' => 7,
                'nama' => 'II/c',
                'kode' => 23,
                'pangkat' => 'Pengatur'
            ],
            [
                'id' => 8,
                'nama' => 'II/d',
                'kode' => 24,
                'pangkat' => 'Pengatur Tingkat I'
            ],
            [
                'id' => 9,
                'nama' => 'III/a',
                'kode' => 31,
                'pangkat' => 'Penata Muda'
            ],
            [
                'id' => 10,
                'nama' => 'III/b',
                'kode' => 32,
                'pangkat' => 'Penata Muda Tingkat I'
            ],
            [
                'id' => 11,
                'nama' => 'III/c',
                'kode' => 33,
                'pangkat' => 'Penata'
            ],
            [
                'id' => 12,
                'nama' => 'III/d',
                'kode' => 34,
                'pangkat' => 'Penata Tingkat I'
            ],
            [
                'id' => 13,
                'nama' => 'IV/a',
                'kode' => 41,
                'pangkat' => 'Pembina'
            ],
            [
                'id' => 14,
                'nama' => 'IV/b',
                'kode' => 42,
                'pangkat' => 'Pembina Tingkat I'
            ],
            [
                'id' => 15,
                'nama' => 'IV/c',
                'kode' => 43,
                'pangkat' => 'Pembina Utama Muda'
            ],
            [
                'id' => 16,
                'nama' => 'IV/d',
                'kode' => 44,
                'pangkat' => 'Pembina Utama Madya'
            ],
            [
                'id' => 17,
                'nama' => 'IV/e',
                'kode' => 45,
                'pangkat' => 'Pembina Utama'
            ]
        ];
        foreach($data as $insert) {
          DB::table('m_golongan')->insert($insert);
        }
    }
}
