<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('m_satuan_kerja', function(Blueprint $table) {
            // $table->foreign('kode_parent')->references('kode')->on('m_satuan_kerja');
            $table->foreign('kecamatan_id')->references('id')->on('m_kecamatan');
            // $table->foreign('eselon_kode')->references('kode')->on('m_eselon');
            $table->foreign('kode')->references('nip')->on('master_user')->onUpdate('cascade');
        });

        Schema::table('m_jabatan', function(Blueprint $table) {
            $table->foreign('eselon_id')->references('id')->on('m_eselon');
            $table->foreign('kode_parent')->references('kode')->on('m_jabatan');
            $table->foreign('jenis_id')->references('id')->on('m_jenis_jabatan');
        });

        Schema::table('m_unit_kerja', function(Blueprint $table) {
            $table->foreign('satuan_kerja_id')->references('id')->on('m_satuan_kerja');
        });

        Schema::table('pegawai', function (Blueprint $table) {
            $table->foreign('jabatan_id')->references('id')->on('m_jabatan');
            $table->foreign('golongan_id')->references('id')->on('m_golongan');
            $table->foreign('satuan_kerja_id')->references('id')->on('m_satuan_kerja');
            $table->foreign('golongan_darah_id')->references('id')->on('m_golongan_darah');
            $table->foreign('agama_id')->references('id')->on('m_agama');
            $table->foreign('nip')->references('nip')->on('master_user');
            $table->foreign('status_kepegawaian_id')->references('id')->on('m_status_kepegawaian');
        });

        Schema::table('m_tupoksi', function (Blueprint $table) {
            $table->foreign('jabatan_id')->references('id')->on('m_jabatan');
        });

        Schema::table('skp', function (Blueprint $table) {
            $table->foreign('skp_output_id')->references('id')->on('m_skp_output');
            $table->foreign('nip')->references('nip')->on('pegawai');
            $table->foreign('tupoksi_id')->references('id')->on('m_tupoksi');
            $table->foreign('kegiatan_id')->references('id')->on('m_kegiatan');
            $table->foreign('tipe_waktu_id')->references('id')->on('m_tipe_waktu');
        });

        Schema::table('input_aktifitas', function (Blueprint $table) {
            $table->foreign('nip')->references('nip')->on('pegawai');
            $table->foreign('skp_id')->references('id')->on('skp');
        });

        Schema::table('m_review', function (Blueprint $table) {
            $table->foreign('kategori_review_id')->references('id')->on('m_kategori_review');
            // $table-
        });

        Schema::table('input_review', function (Blueprint $table) {
            $table->foreign('review_id')->references('id')->on('m_review');
            $table->foreign('nip')->references('nip')->on('pegawai');
            $table->foreign('nip_reviewer')->references('nip')->on('pegawai');
        });

        Schema::table('reviewer', function (Blueprint $table) {
            $table->foreign('nip')->references('nip')->on('pegawai');
            $table->foreign('nip_reviewer')->references('nip')->on('pegawai');
        });

        Schema::table('m_aktifitas', function (Blueprint $table) {
            $table->foreign('kategori_aktifitas_id')->references('id')->on('m_kategori_aktifitas');
            $table->foreign('skp_output_id')->references('id')->on('m_skp_output');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
