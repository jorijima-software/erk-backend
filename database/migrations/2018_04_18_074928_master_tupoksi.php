<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Migration for Master Tupoksi
 */
class MasterTupoksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_tupoksi', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->string('kode')->unique();
            $table->string('nama');
            $table->boolean('utama')->default(true);
            $table->integer('jabatan_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_tupoksi');
    }
}
