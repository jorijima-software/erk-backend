<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Eselon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_eselon', function(Blueprint $table) {
            $table->increments('id')->unique();
            $table->string('nama');
            $table->string('kode')->unique();
            $table->integer('golongan_awal_id');
            $table->integer('golongan_akhir_id');
        });
        // Schema::table('m_satuan_kerja', function(Blueprint $table) {
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_eselon');
    }
}
