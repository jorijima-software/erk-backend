<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MasterKegiatan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_kegiatan', function(Blueprint $table) {
            $table->increments('id')->unique();
            $table->string('nama');
            $table->string('kode')->unique();
            $table->boolean('utama')->default(true);
            $table->float('tingkat_kesulitan')->default(1.0);
            $table->integer('biaya')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_kegiatan');
    }
}
