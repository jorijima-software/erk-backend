<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MasterSkp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Sasaran Kerja Pegawai
        Schema::create(
            'skp', function(Blueprint $table) {
                $table->increments('id')->unique();
                $table->string('nip');
                $table->integer('tupoksi_id')->unsigned()->nullable();
                $table->integer('kegiatan_id')->unsigned()->nullable();
                $table->string('nama'); // Nama SKP
                $table->integer('waktu')->default(12); // Waktu Dalam Bulan
                $table->integer('tipe_waktu_id')->default(3)->unsigned(); // Waktu Dalam Bulan
                $table->string('output'); // Output / Yang Diharapkan
                $table->integer('skp_output_id')->unsigned();
                $table->integer('biaya');
                $table->integer('kualitas')->default(0);
                $table->timestamps();
            }
        );


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('skp');
    }
}
