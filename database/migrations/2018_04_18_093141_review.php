<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Review extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'input_review', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('skor');
                $table->string('nip');
                $table->string('nip_reviewer');
                $table->integer('review_id')->unsigned();
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('input_review');
    }
}
