<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SatuanKerja extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_satuan_kerja', function(Blueprint $table) {
            $table->increments('id')->unique();
            $table->string('kode')->unique();
            $table->string('kode_parent')->nullable();
            $table->string('nama');
            $table->string('eselon_kode')->nullable();
            $table->string('alamat')->nullable();
            $table->integer('kecamatan_id')->unsigned()->nullable();
            $table->integer('status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_satuan_kerja');
    }
}
