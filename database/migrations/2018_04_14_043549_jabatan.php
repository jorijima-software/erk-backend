<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Jabatan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_jabatan', function(Blueprint $table) {
            $table->increments('id')->unique();
            $table->string('nama');
            $table->string('kode')->unique();
            $table->string('kode_parent')->nullable();
            $table->integer('eselon_id')->unsigned()->nullable();
            $table->integer('jenis_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_jabatan');
    }
}
