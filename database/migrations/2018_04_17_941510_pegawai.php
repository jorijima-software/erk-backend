<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pegawai extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pegawai', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->string('nip')->unique();
            $table->string('nama');
            $table->date('tanggal_lahir');
            $table->string('tempat_lahir');
            $table->integer('satuan_kerja_id')->unsigned();
            $table->integer('jabatan_id')->unsigned()->nullable();
            $table->integer('golongan_id')->unsigned()->nullable();
            $table->string('alamat')->nullable();
            $table->string('no_telp')->nullable();
            $table->integer('golongan_darah_id')->unsigned()->nullable();
            $table->integer('instansi_id')->unsigned()->nullable();
            $table->integer('agama_id')->unsigned()->nullable();
            $table->integer('status_kepegawaian_id')->unsigned()->nullable();
            $table->integer('jk'); // 1 = Laki-Laki, 2 = Perempuan
            $table->integer('umur_pensiun')->default(58);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pegawai');
    }
}
