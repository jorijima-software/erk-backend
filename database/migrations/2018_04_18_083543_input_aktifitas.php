<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Console\Scheduling\Schedule;

class InputAktifitas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'input_aktifitas', function (Blueprint $table) {
                $table->increments('id')->unique();
                $table->string('nip');
                $table->string('nama');
                $table->integer('skp_id')->unsigned();
                $table->dateTime('waktu_mulai');
                $table->dateTime('waktu_selesai');
                $table->string('keterangan')->nullable();
                $table->integer('hasil')->default(0);
                /**
                 * 1: Diproses
                 * 2: Accept
                 * 0: Ditolak
                 */
                $table->integer('status')->default(1);
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('input_aktifitas');
    }
}
