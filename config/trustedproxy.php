<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 4/14/18
 * Time: 4:09 PM
 */
return [
    'proxies' => null,
    'headers' => Illuminate\Http\Request::HEADER_X_FORWARDED_ALL,
];