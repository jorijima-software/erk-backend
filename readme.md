To Install this software you'll need to meet the minimum requirement:
- PHP 7.1.4 or *greater*
- Composer
- MySQL Database

After you meet the minimum requirement, you'll need to do
- `composer install`
- `cp .env.example .env` then change the setting to your liking
- `php artisan key:generate`
- `php artisan jwt:secret`
- `php artisan migrate:fresh`
- `php artisan db:seed`
- For production it's recommended to change `.env` DEBUG to `false`
- For development it's recommended to use port 6900 instead of 8000, to do so use `php artisan serve --port 6900`

# Don't forget to type the correct database 😂😂😂